# Vue cli基础
>什么是vue-cli

Vue-cli是Vue官方提供的用来搭建项目的脚手架工具，它是Vue.js开发标准工具。集成了webpack，内置了好多常用的配置使得开发过程更加标准化。

>vue-cli安装

```
npm install -g @vue/cli@3.9.3

//查看当前vue版本
vue --version
```
## vue-cli创建项目
```
vue create <project_name>
```
## vue-cli脚手架项目结构
```
|-- node_modules: 存放下载依赖的文件夹
|-- public: 存放不会变动静态的文件，它与src/assets的区别在于，public目录中的文件不被webpack打包处理，会原
样拷贝到dist目录下
|-- index.html: 主页面文件
|-- favicon.ico: 在浏览器上显示的图标
|-- src: 源码文件夹
|-- assets: 存放组件中的静态资源
|-- components: 存放一些公共组件
|-- views: 存放所有的路由组件
|-- App.vue: 应用根主组件
|-- main.js: 应用入口 js
|-- .browserslistrc: 指定了项目可兼容的目标浏览器范围, 对应是package.json 的 browserslist选项
|-- .eslintrc.js: eslint相关配置
|-- .gitignore: git 版本管制忽略的配置
|-- babel.config.js: babel 的配置,即ES6语法编译配置
|-- package-lock.json: 用于记录当前状态下实际安装的各个包的具体来源和版本号等, 保证其他人在 npm install 项
目时大家的依赖能保证一致.
|-- package.json: 项目基本信息,包依赖配置信息等
|-- postcss.config.js: postcss一种对css编译的工具，类似babel对js的处理
|-- README.md: 项目描述说明的 readme 文件

```
## vue-cli自定义配置
在项目根目录下创建vue.config.js,具体参数可参考: https://cli.vuejs.org/zh/config/#vue-config-js

```
module.exports = {
    devServer: {
    port: 8001, // 端口号，如果端口被占用，会自动提升 1
    open: true, // 启动服务自动打开浏览器
    https: false, // 协议
    host: "localhost", // 主机名，也可以 127.0.0.1 或 做真机测试时候 0.0.0.0
    },
    lintOnSave: false, // 默认 true, 警告仅仅会被输出到命令行，且不会使得编译失败。
    outputDir: "dist2", // 默认是 dist ,存放打包文件的目录,
    assetsDir: "assets", // 存放生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir) 目录
    indexPath: "out/index.html", // 默认 index.html, 指定生成的 index.html 的输出路径 (相对于 outputDir)。
    productionSourceMap: false, // 打包时, 不生成 .map 文件, 加快打包构建
    };
```
## eslint语法插件
ESlint是一个语法规则和代码风格的检查工具，可以用来保证写出语法正确，风格统一的代码。
>ESlint配置

在项目根目录下package.json中eslintConfig选项配置或者.eslintrc.js配置
```
"eslintConfig": {
    "root": true,
    "env": {
      "node": true
    },
    "extends": [
      "plugin:vue/essential",
      "eslint:recommended"
    ],
    "rules": {},
    "parserOptions": {
      "parser": "babel-eslint"
    }
  },
```
>自定义语法规则

eslint官方规则参考：https://cneslint.org/docs/rules/

语法：
```
rules: {
    "规则名": [错误等级值，规则配置]
}
```
规则值：
```
off或者0， 关闭规则
warn或者1，警告
error或者2，错误
```
执行下面命令对根据ESlint定义的语法规则进行检查，并按语法规则进行自动修复项目中不合规的代码
```
npm run lint
```
# 会员管理系统项目
>前后端分离框架开发

1.前后端开发约定好APi接口，数据，参数
2.前后端并行开发
## RESTFul架构风格
通过HTTP请求方式(get,post,put,delete)来区分对资源的增删改查，请求URL `/资源名称/资源标识`

定义接口方式
|名称|请求方式|RESTFul API|
|:---:|:---:|:---:|
|查询|get|member/list|
|增加|post|member/{id}|
|修改|put|member/{id}|
|删除|delete|member/{id}|
## Mock.js数据生成器
同一通过模拟数据生成器，通过一定规则APiece文档生成模拟数据接口，提供给前端人员进行测试。

创建mockjs-demo项目
```
mkdir mockjs-demo
npm init
```

安装mockjs
```
npm install -D mockjs
```
创建demo.js
```
const Mock = require('mockjs')

const data = Mock.mock({
    //定义数据生成规则
    'memberList|4':[
        {
            'id': 1,
            'name': '小梦'
        }
    ]
})
console.log(JSON.stringify(data,null,2)) //参数2，不同级别两个空格占位符
```
执行node命令测试
```
node demo.js
```
## Mockjs语法规则
Mock.js的语法规范包括两个部分，数据模板定义规范和数据占位符定义规范
### 数据模板定义规范DTD
数据模板由三个部分组成，属性名、生成规则，属性值
```
'name|rule':value
```
生成规则共有7种格式：
```
'name|min-max':value
'name|count':value
'name|min-max.dmin-dmax':value
'name|min-max.dcount':value
'name|count.dmin-dmax':value
'name|count.dcount':value
'name|+step':value
```
### 数据占位符定义规范DPD
Mock.Random是一个工具类，用于生成各种随机数据。数据占位符格式：
```
'属性名':@占位符
```
mock.Random提供完整占位符如下：
|类型|占位符|
|:---:|:---:|
|basic|boolean,natural,integer,float,character,string,range|
|date|date(年月日),time(时分秒),datetime(年月日时分秒)|
|image|image,dataimage|
|color|color|
|text|paragraph,sentence,word,title,cparagraph,csentence,cword,ctitle|
|name|first,last,name,cfirst,clast,cname|
|web|url,domain,email,ip,tid|
|address|area,region|
|helper|capitalize,upper,lower,pick,shuffle|
|miscellaneous|guid,id|
>基本类型占位符

随机生成基本数据类型的数据，占用的占用符：natural、integer、string、float、boolean
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp|10':[
        {
            'id|+1': 1,
            'name':'@cname',
            'price': '@float',
            'status': '@boolean'
        }
    ]
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": {
    "id": 1,
    "name": "魏军",
    "price": 1089196328578832.9,
    "status": true
  }
}

```
>日期占位符

随机生成日期类型的数据，占位符：date/date(format)、time/time(format)、datetime/datetime(format)
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':[
        {
            'birthday': '@date',
            'entryDate': '@date("yyyy/MM/dd")',
            'createDate': '@datetime',
            'updateDate': '@datetime("yyy/MM/dd HH:mm:ss")'
        }
    ]
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": [
    {
      "birthday": "2014-11-08",
      "entryDate": "2012/01/12",
      "createDate": "2014-09-25 20:43:04",
      "updateDate": "0505/08/07 01:21:32"
    }
  ]
}
```
>图像占位符

随机生成图片地址，生成的图片浏览器可打开；占位符：image
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':[{
        'img': '@image'
    }]
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": [
    {
      "img": "http://dummyimage.com/728x90"
    }
  ]
}
```
>文本占位符

随机生成一段文本，占位符：ctitle、csentence(mix,max)
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':{
        'title': '@ctitle(3,6)',
        'content': '@csentence(8,12)'
    }
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": {
    "title": "文交适例形",
    "content": "除你次争合说后构单好。"
  }
}
```
>名称占位符

可随机生成名字，占位符：first英文名、last英文姓、name英文姓名、cfirst中文名、clast中文姓、cname中文姓名
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':{
        
        'en_name': '@name',
        'cn_name': '@cname',
    }
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": {
    "en_name": "Charles Taylor",
    "cn_name": "黎军"
  }
}
```
>网络占位符

可随机生成url、域名、ip地址、邮件地址，占位符：url(protocol,host)生成URL(protocol协议http/https，host域名和端口号)、domain生成域名、ip生成Ip地址、email
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':{

        'url': '@url(https)',
        'domain': '@domain',
        'email': '@email'
    }
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": {
    "url": "https://cwotj.pa/jxisrfpj",
    "domain": "eeqmluv.td",
    "email": "k.zxqzb@ljbr.asia"
  }
}
```
>地址占位符

可随机生成区域、省市县、邮政编码，占位符：region区域、county省市县、zip邮政编码
```
const Mock = require('mockjs')
const data = Mock.mock({
    'emp':{

        'region': '@region', //随机生成大区
        'province': '@province', //随机生成省会城市
        'city': '@city(true)',  //随机生成城市,如果为true，则生成省份城市加地级市
        'county': '@county(true)',  //随机生成县，如果为true，则生成省、市、区
        'zip': '@zip'
    }
})
console.log(JSON.stringify(data,null,2))
```
结果：
```
{
  "emp": {
    "region": "华中",
    "province": "湖南省",
    "city": "福建省 厦门市",
    "county": "内蒙古自治区 乌海市 乌达区",
    "zip": "454137"
  }
}
```
# EasyMock
参考连接mock.mengxuegu.com
#会员管理系统项目
## 创建项目
安装vue
```
npm install -g @vue/cli@3.10.0
```
创建vue项目
```
vue create vue-cms
```
```
Vue CLI v3.10.0
? Please pick a preset:
  default (babel, eslint)
> Manually select features
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project:
 (*) Babel
 ( ) TypeScript
 ( ) Progressive Web App (PWA) Support
 (*) Router
 ( ) Vuex
>(*) CSS Pre-processors
 (*) Linter / Formatter
 ( ) Unit Testing
 ( ) E2E Testing
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, CSS Pre-processors, Linter
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): (Use arrow keys)
> Sass/SCSS (with dart-sass)
  Sass/SCSS (with node-sass)
  Less
  Stylus
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, CSS Pre-processors, Linter
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config:
  ESLint with error prevention only
  ESLint + Airbnb config
  ESLint + Standard config
> ESLint + Prettier
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, CSS Pre-processors, Linter
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)
>(*) Lint on save
 ( ) Lint and fix on commit
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, CSS Pre-processors, Linter
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)Lint on save
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? (Use arrow keys)
> In dedicated config files
  In package.json
```
```
Vue CLI v3.10.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, CSS Pre-processors, Linter
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)Lint on save
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: vue-cms
```
## 项目初始化
>更改标题

编辑public/index.html
```
<title>vue-cms会员管理系统</title>
```
>配置devServer

在项目根目录下创建vue.config.js
```
module.exports = {
    devServer: {
        port: 8080,
        host: '127.0.0.1',
        https: true,
        open: false //启动项目自动打开浏览器
    },
    lintOnSave: false, //关闭格式检查
    productionSourceMap: false //打包不会生成.map文件,加快打包速度
}
```
>整合第三方库

安装axios，处理异步请求
```
npm i -S axios@0.19.0
```
安装pubsub，处理vue单组件，非父子组件通信
```
npm i -S pubsub-js@1.7.0
```
## 整合elementUI
安装elementui
```
npm i -S element-ui@2.11.1
```
>引入elementui

编辑项目main.js
```
import ElementUI from 'element-ui';
//使用element ui
Vue.use(ElementUI);
```
# Axios封装
## 封装axios对象

>定义axios封装对象
项目中很多组件要通过axios发送异步请求，所以封装一个axios对象。自己封装的axios在后续可以使用axios中提供的拦截器

在src/utils/request.js文件中
```
import axios from 'axios';

// eslint-disable-next-line no-unused-vars
const  axios_request = axios.create({
    baseURI: 'https://mock.mengxuegu.com/mock/627ce1254554ef2f96d4ef5e/allen',
    timeout: 5000
})

export default axios_request //导出自定义创建axios对象
```
>使用封装的axios对象

在src/api/test.js中调用axios_request对象
```
import axios_request from "@/utils/request";

axios_request({
    method: 'get',
    url:'api/1234'
}).then(response => {
    console.log(response.data)
})
```
在src/components/HelloWorld.vue组件中引入test.js，使用浏览器测试。
```
<script>
    import testApi from '@/api/test'
</script>
```
## 跨域问题解决
在vue.config.js中定义
```
module.exports = {
    devServer: {
        proxy:{ //代理配置，解决跨域问题
            'dev-api':{
                target: 'http://localhost:8001',
                changeOrigin: true, //开启代理
                pathRewrite: {
                    '^/dev-api':'', //将请求地址替换给空
                }
            }
        }
    },
}
```
>不同环境配置跨域代理

在.env.development配置
```
# api接口地址
VUE_APP_SERVER_URL = 'http://localhost:8001'
# api接口前缀
VUE_APP_BASE_API = 'dev-api'
```
在vue.config.js中引用
```
proxy:{ //代理配置，解决跨域问题
    [process.env.VUE_APP_BASE_API]:{
        target: process.env.VUE_APP_SERVER_URL,
        changeOrigin: true, //开启代理
        pathRewrite: {
            // '^/dev-api':'', //将请求地址替换给空
            ['^' + process.env.VUE_APP_BASE_API ]: ''
        }
    }
}
```
# 系统登录管理
>需求

用户名密码校验完成跳转到首页

## 创建login组件
在views/login/index.vue
```
<template>
  <div>
    登录页面
  </div>
</template>
```
## 配置路由
```
import Vue from "vue";
import Router from "vue-router";
// import Login from "./views/login/index.vue"
import Login from "./views/login" //默认会导入./views/login/index.vue组件
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
```
在App.vue渲染路由
```
<template>
  <div id="app">
    <router-view></router-view>
  </div>
</template>
```
浏览器访问https://127.0.0.1:8080/#/login测试
## Element UI实现登录页面
main.js引入Element UI
```
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

//使用element ui
Vue.use(ElementUI);
```

>引入elementui没有样式

对postcss-loader版本降级：
```
npm install postcss-loader@4.0.4
```
views/login/index.vue
```
<template>
  <div class="login-container">
    <el-form class="login-form" ref="form" :model="form" label-width="80px">
      <h2 class="login-title">CMS会员管理系统</h2>
      <el-form-item label="账户">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <el-form-item label="密码">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <div class="form-commit">
        <el-form-item>
          <el-button type="primary" @click="onSubmit">立即登录</el-button>
          <el-button>忘记密码</el-button>
        </el-form-item>
      </div>
    </el-form>
  </div>
</template>

<style>
.login-container{
  background-image: url("../../assets/login.jpeg");
  position: absolute;
  width: 100%;
  height: 100%;
  /*滚动条隐藏*/
  overflow: hidden;
}
.login-form{
  width: 350px;
  margin: 160px auto;
  background-color: rgba(255,255,255,0.8);
  padding: 30px;
  border-radius: 20px;
}
.login-title{
  text-align: center;
  color: #303133;
}
</style>

<script>
export default {
  data() {
    return {
      form: {
        name: '',
      }
    }
  },
  methods: {
    onSubmit() {
      console.log('submit!');
    }
  }
}
</script>
```
## 账户密码表单校验
Form组件提供了表单校验规则，只需要rules属性传入约定规则。并将Form-item的prop属性设置为需校验的字段名即可。
```
<template>
  <div class="login-container">
<!--    通过rules属性绑定rules规则-->
    <el-form class="login-form" :rules="rules" ref="form" :model="form" label-width="80px">
      <h2 class="login-title">CMS会员管理系统</h2>
      <el-form-item label="账户" prop="username">
        <el-input v-model="form.username"></el-input>
      </el-form-item>
      <el-form-item label="密码" prop="password">
        <el-input v-model="form.password"></el-input>
      </el-form-item>
      <div class="form-commit">
        <el-form-item>
          <el-button type="primary" @click="submitForm('form')">立即登录</el-button>
<!--          submitForm函数绑定form是表单ref属性-->
          <el-button>忘记密码</el-button>
        </el-form-item>
      </div>
    </el-form>
  </div>
</template>
```
```
<style>
.login-container{
  background-image: url("../../assets/login.jpeg");
  position: absolute;
  width: 100%;
  height: 100%;
  /*滚动条隐藏*/
  overflow: hidden;
}
.login-form{
  width: 350px;
  margin: 160px auto;
  background-color: rgba(255,255,255,0.8);
  padding: 30px;
  border-radius: 20px;
}
.login-title{
  text-align: center;
  color: #303133;
}
.form-commit{
  margin: 10px;
}
</style>
```
```
<script>
export default {
  data() {
    return {
      form: {
        username: '',
        password: ''
      },
      rules:{
        username: [
          {required: true,message: '请输入账户名称',trigger: 'blur'}
        ],
        password: [
          {required: true,message: '请输入账户密码',trigger: 'blur'}
        ]
      }
    }
  },
  methods: {
    submitForm(formName){
      this.$refs[formName].validate((valid=>{
        if (valid){
          alert("submit")
        }else {
          console.log('error submit')
          return false
        }
      }))
    }
  }
}
</script>
```
## 添加login登录后台接口
>配置API接口地址

编辑.env.development
```
# api接口地址
VUE_APP_SERVER_URL = 'http://127.0.0.1:8000'
# api接口前缀,比如api访问地址http://127.0.0.1:8000/api/xxx，则VUE_APP_BASE_API = 'api'
VUE_APP_BASE_API = 'api'
```
>API数据约定

用户登录
```
URL: /user/login
method: post
响应数据：
{
    "code":2000,
    "flag": true,
    "message": "login success",
    "data":{
        "token":{user_id}
    }
}
```
python视图实现
```
class UserLogin(View):
    def post(self,request):
        #获取request post数据使用request.body方法，decode将bytes数据转换为str
        user = json.loads(request.body.decode())['username']
        if user == "admin":
            resp = {
                "code": 2000,
                "flag": "true",
                "message": "login success",
                "data":{
                    "token":user
                }
            }
        else:
            resp = {
                "code": 2001,
                "flag": "false",
                "message": "user %s is not found" %user,
                "data":{
                    "token":user
                }
            }

        return JsonResponse(resp)
```
响应用户信息
```
URL: /user/info/{token}
method: get
响应数据：
{
    "code":2000,
    "flag": true,
    "message": "login success",
    "data":{
        "id":{user_id},
        "name": "user_name",
        "rule": "manage"
    }
}
```
python视图实现
```
class UserInfo(View):
    def get(self,request,pk):
        resp = {
            "code": 2000,
            "flag": "true",
            "message": "login success",
            "data":{
                "id":pk,
                "name": "admin",
                "rule": "manage"
            }
        }
        return JsonResponse(resp)
```
编辑vue.config.js设置代理
```
proxy:{ //代理配置，解决跨域问题
    [process.env.VUE_APP_BASE_API]:{
        target: process.env.VUE_APP_SERVER_URL,
        changeOrigin: true, //开启代理
        pathRewrite: {
            // '^/dev-api':'', //将请求地址替换给空
            ['^' + process.env.VUE_APP_BASE_API ]: ''
        }
    }
}
```
编辑src/utils/request.js
```import axios from 'axios';

// eslint-disable-next-line no-unused-vars
const  axios_request = axios.create({
    baseURI: 'http://127.0.0.1:8000/api',
    timeout: 5000
})

export default axios_request //导出自定义创建axios对
```
>前端登录逻辑实现

在src/api/login.js定义登录逻辑处理
```
import request from "@/utils/request";

export function login(username,password){
    return request({
        url: 'api/user/login/',
        method: 'post',
        data:{
            username,
            password
        }
    })
}

export function getUserInfo(token){
    return request({
        url:`api/user/info/${token}`,
        methods: 'get'
    })
}
```
在src/views/login/index.vue中submitForm处理
```
<script>
import {login,getUserInfo} from "@/api/login";

export default {
  data() {
    return {
      form: {
        username: '',
        password: ''
      },
      rules:{
        username: [
          {required: true,message: '请输入账户名称',trigger: 'blur'}
        ],
        password: [
          {required: true,message: '请输入账户密码',trigger: 'blur'}
        ]
      }
    }
  },
  methods: {
    submitForm(formName){
      this.$refs[formName].validate((valid=>{
        if (valid){
          //验证账户密码是否通过
          login(this.form.username,this.form.password).then(response=>{
            const resp = response.data
            if(resp.flag === "true"){
              //通过获取用户信息异步请求
              getUserInfo(resp.data.token).then(response=>{
                //存入session
                const respUser = response.data
                if(respUser.flag){
                  //将信息保存到localStorage中
                  localStorage.setItem('user',JSON.stringify(respUser.data))
                  //方便后面验证
                  localStorage.setItem('token',JSON.stringify(resp.data.token))
                  //登录成功之后跳转到首页
                  this.$router.push('/')
                }else {
                  //登录失败获取信息弹出警告
                  this.$message({
                    message: respUser.message,
                    type: "warning"
                  })
                }
              })
            }else {
              this.$message({
                message: resp.message,
                type: "error"
              })
            }
          })
        }else {
          //未通过弹出警告
          console.log('submit error')
        }
      }))
    }
  }
}
</script>
```
# 项目布局和权限
## 页面布局
>配置路由

编辑src/router.js
```
import Vue from "vue";
import Router from "vue-router";
// import Login from "./views/login/index.vue"
import Login from "./views/login" //默认会导入./views/login/index.vue组件
import Home from "./views/home"
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Home
    }
  ]
})
```
编辑src/views/home/index.vue
```
<template>
  <div>
      <div class="app-header">头部</div>
      <div class="app-navbar">导航</div>
      <div class="app-main">主页面</div>
  </div>
</template>
<!--scoped属性，只作用到当前组件-->
<style scoped>
.app-header{
  position: absolute;
  line-height: 50px;
  background-color: darkturquoise;
  /*左边0边距*/
  left: 0px;
  /*右边0边距，左右100%占满*/
  right: 0px;
  top: 0px;
}
.app-navbar{
  position: absolute;
  bottom: 0px;
  top: 50px;
  left: 0px;
  padding: 0px;
  background-color: #42b983;
  width: 200px;
}
.app-main{
  position: absolute;
  background-color: antiquewhite;
  left: 200px;
  right: 0px;
  bottom: 0px;
  top: 50px;
}
</style>
```
>页面布局抽取为vue组件

编辑src/views/home/index.vue
```
<template>
  <div>
      <app-header></app-header>
      <app-navbar></app-navbar>
      <app-main></app-main>
  </div>
</template>
<script scoped>
import AppHeader from "@/components/header"
import AppNavbar from "@/components/navbar"
import AppMain from "@/components/main"
export default {
  components: {
    AppHeader,AppNavbar,AppMain
  }
}
</script>
```
在src/components/header.vue
```
<template>
  <div class="app-header">头部</div>
</template>
```
在src/components/navbar.vue
```
<template>
  <div class="app-navbar">导航</div>
</template>
```
在src/components/main.vue
```
<template>
  <div class="app-main">主页面</div>
</template>

```
>导航栏右侧用户中心实现

编辑src/components/header.vue
```
<template>
  <div class="app-header">
    <el-dropdown trigger="click">
      <span class="el-dropdown-link">
        admin<i class="el-icon-arrow-down el-icon--right"></i>
      </span>
      <el-dropdown-menu slot="dropdown">
        <el-dropdown-item icon="el-icon-user">个人中心</el-dropdown-item>
        <el-dropdown-item icon="el-icon-setting">修改密码</el-dropdown-item>
        <el-dropdown-item icon="el-icon-unlock">退出登录</el-dropdown-item>
      </el-dropdown-menu>
    </el-dropdown>
  </div>
</template>
<style scoped>
.el-dropdown{
  float: right;
  margin-right: 40px;
}
.el-dropdown-link{
  cursor: pointer;
  color: #fff;
}
</style>
```
>左侧菜单栏

编辑src/components/navbar.vue
```
<template>
  <div class="app-navbar">
    <!--        开启路由功能之后index指定路由地址，default-active默认选中菜单为黄色，注意v-bind:default-active才可以指定表达式-->
    <el-menu
        :router="true"
        default-active="$route.path"
        class="el-menu-vertical-demo"
        @open="handleOpen"
        @close="handleClose"
        background-color="#545c64"
        text-color="#fff"
        active-text-color="#ffd04b">
      <el-menu-item index="dashboard">
        <i class="el-icon-s-home"></i>
        <span slot="title">DashBoard</span>
      </el-menu-item>
      <el-menu-item index="member">
        <i class="el-icon-user"></i>
        <span slot="title">会员管理</span>
      </el-menu-item>
      <el-menu-item index="supplier">
        <i class="el-icon-s-shop"></i>
        <span slot="title">供应商管理</span>
      </el-menu-item>
      <el-menu-item index="goods">
        <i class="el-icon-goods"></i>
        <span slot="title">商品管理</span>
      </el-menu-item>
      <el-menu-item index="staff">
        <i class="el-icon-user-solid"></i>
        <span slot="title">员工管理</span>
      </el-menu-item>
    </el-menu>
  </div>
</template>
<style scoped>
.el-menu {
  /*隐藏右侧外边框*/
  border-right: none;
}
</style>
<script>
export default {
  methods: {
    handleOpen(key, keyPath) {
      console.log(key, keyPath);
    },
    handleClose(key, keyPath) {
      console.log(key, keyPath);
    }
  }
}
</script>
```
>左侧菜单栏路由配置

在views目录下分别创建dahsbord、goods、member、staff视图

```
import Vue from "vue";
import Router from "vue-router";
// import Login from "./views/login/index.vue"
import Login from "./views/login"; //默认会导入./views/login/index.vue组件
import Layout from "./views/layout";
import Dashboard from "./views/dashboard";
import Goods from "./views/goods";
import Member from "./views/member";
import Staff from "./views/staff";
import Supplier from "./views/supplier";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'layout',
      component: Layout,
      redirect: '/dashboard', //重定向到子路由
      children:[
        {
          path:'/dashboard',
          component: Dashboard,
          meta: {title:'dashboard'}
        }
      ]
    },
    {
      path: '/member',
      component: Layout,  //渲染布局
      children: [
        {
          path: '/',
          component: Member,
          meta: {title: 'member'}
        }
      ]
    },
    {
      path: '/staff',
      component: Layout,
      children: [
        {
          path: '/',
          component: Staff,
          meta: {title: 'staff'}
        }
      ]
    },
    {
      path: '/goods',
      component: Layout,
      children: [
        {
          path: '/',
          component: Goods,
          meta: {title: 'goods'}
        }
      ]
    },
    {
      path: '/supplier',
      component: Layout,
      children: [
        {
          path: '/',
          component: Supplier,
          meta: {title: 'supplier'}
        }
      ]
    }
  ]
})
```
编辑页面布局main组件当访问菜单栏比如/dashboard时渲染main组件，编辑src/components/main.vue
```
<template>
  <div class="app-main">
    <router-view></router-view>
  </div>
</template>

```
>主页面面包屑

编辑main.vue
```
<template>
  <div class="app-main">
    <el-breadcrumb separator-class="el-icon-arrow-right">
      <el-breadcrumb-item :to="{ path: $route.path }">{{$route.meta.title}}</el-breadcrumb-item>
    </el-breadcrumb>
    <router-view></router-view>
  </div>
</template>

<style scoped>
  .el-breadcrumb{
    height: 10px;
    padding: 20px;
    border-radius: 4px;
    box-shadow: 0 2px 12px rgba(0,0,0,0.1);
  }
  .el-breadcrumb__item{
    border-left: 3px solid #31c17b;
    padding-left: 10px;
  }
</style>

```
将面包屑抽取为Link组件

main.vue
```
<template>
  <div class="app-main">
    <app-link v-show="$route.path != '/dashboard'"></app-link>
<!--    判断当访问路径为/dashboard时不显示面包屑-->
    <router-view></router-view>
  </div>
</template>
<script>
import AppLink from "@/components/link"
export default {
  components:{AppLink}
}
</script>
```
src/componments/link.vue
```
<template>
  <div>
    <el-breadcrumb separator-class="el-icon-arrow-right">
      <el-breadcrumb-item :to="{ path: $route.path }">{{ $route.meta.title }}</el-breadcrumb-item>
    </el-breadcrumb>
  </div>
</template>

<style scoped>
  .el-breadcrumb{
    height: 10px;
    padding: 20px;
    border-radius: 4px;
    box-shadow: 0 2px 12px rgba(0,0,0,0.1);
  }
  .el-breadcrumb__item{
    border-left: 3px solid #31c17b;
    padding-left: 10px;
  }
</style>
```
## 账户注销

>注销账户接口约束

|名称|描述|
|:---|:---|
|请求URL|/user/logout|
|请求方式|post|
|提交参数|data:{token}|
|描述|注销账户|
完成返回数据
```
{
    "code": 2000,
    "flag": "true",
    "message": "账户退出成功"
}
```
>定义logout函数

编辑src/api/logout.js
```
import request from "@/utils/request";

export function logout(token){
    return request({
        url: '/user/logout',
        method: 'post',
        data:{
            token
        }
    })
}
```
>点击header退出登录按钮实现用户退出

编辑src/components/header.vue
```
<template>
  <div class="app-header">
    <el-dropdown @command="handleCommand">
  <span class="el-dropdown-link">
    admin<i class="el-icon-arrow-down el-icon--right"></i>
  </span>
      <el-dropdown-menu slot="dropdown">
        <el-dropdown-item command="a">个人中心</el-dropdown-item>
        <el-dropdown-item command="b">退出登录</el-dropdown-item>
      </el-dropdown-menu>
    </el-dropdown>
  </div>
</template>
<script>
import {logout} from '@/api/logout'
  export default {
    methods: {
      handleCommand(command) {
        switch (command){
          case 'a':
            //修改密码
              break;
          case 'b':
            //退出登录
              const token = localStorage.getItem('token')
              logout(token).then(response =>{
                const resp = response.data
                if(resp.flag){
                  //退出成功
                  //清除本地数据
                  localStorage.removeItem('token')
                  localStorage.removeItem('user')
                  //跳转到登录页面
                  this.$router.push('/login')

                }else {
                  //退出失败
                  this.$message({
                    message: resp.message,
                    type: 'warning'
                  })
                }
              })

              break;
        }
      }
    }
  }
</script>
```
# 权限校验

>需求

当用户未登录时只能访问login页面，访问其他页面重定向到login页面
>实现

采用Vue Router中的路由前置钩子函数beforeEach，在前置中根据路由地址校验是否此路由允许访问
>代码实现

在src/api/permission.js
```
import router from '@/router'
import getUserInfo from '@/api/login'


//前置路由钩子函数参数：
//to：要进入的目标路由u第项
//from：当前导航要离开的路由对象
//next：调用该方法进入目标路由

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token')
    console.log('token',token)

    if(token){
        //如果有token
        const userInfo = localStorage.getItem('user')
        //获取用户信息
        if (userInfo){
            //如果存在用户信息允许访问
            next()
        }else {
            //如果不存在用户信息则重新获取用户信息
            getUserInfo(token).then((response)=>{
                const resp = response.data
                console.log('resp',resp)
                if(resp.flag){
                    //如果获取到用户信息则将用户信息保存到本地
                    localStorage.setItem('user',JSON.stringify(resp.data))
                }else {
                    //如果不能获取用户信息则重定向到/login页面
                    next({path:'/login'})
                }
            })
        }
    }else {
        //如果没有token
        if (to.path != '/login'){
            //如果没有token且访问路径不是/login全部重定向到/login
            next({path:'/login'})
        }else {
            //如果有token，不会对访问路径拦截
            next()
        }
    }
})
```
引用权限拦截，在项目根目录下main.js中引入permission.js
```
//引入权限校验
import "@/api/permission"
```
# 会员管理
## 列表查询
>需求

针对会员管理列表页面实现列表功能、下拉框、日期、数据分页等功能
>会员列表API接口约束

|名称|描述|
|:---|:---|
|请求URL|/members/list|
|请求方式|get|

接口：
```
{
"code":2000,
"flag":"true",
"message":"会员列表查询成功",
"data":[
    {
    "id":10,
    "cardNum":4835732934010751,
    "name":"魏明",
    "birthday":"1985-07-22",
    "phone":47388740234,
    "integral":"116.00",
    "money":"421.52",
    "payType":1,
    "address":"广西壮族自治区 来宾市 其它区"
    }]
}
```
>调用API接口封装getMemberList函数

编辑src/api/member.js
```
import request from "@/utils/request";
export default {
    getList(){
        return request({
            url: 'api/members/list',
            method: 'get'
        })
    }

}
```
>会员列表页面获取数据js

编辑src/views/member/index.vue
```
<script>
import getMemberList from '@/api/member'
export default {
  data(){
    return {
      list:[]
    }
  },
  //钩子函数获取数据
  created() {
    this.fetchData()
  },
  methods:{
    fetchData(){
      getMemberList.getList().then(response=>{
        this.list = response.data.data
      })
    },
    handleEdit(id){
      console.log('edit',id)
    },
    handleDelete(id){
      console.log('delete',id)
    }

  }
}
</script>
```
>会员列表页面模板

编辑src/views/member/index.vue
```
<template>
  <div>
    <el-table :data="list" height="600" border style="width: 100%">
      <el-table-column prop="id" label="ID" width="60"></el-table-column>
      <el-table-column prop="cardNum" label="卡号" width="180"></el-table-column>
      <el-table-column prop="name" label="姓名" width="120"></el-table-column>
      <el-table-column prop="birthday" label="生日" width="120"></el-table-column>
      <el-table-column prop="phone" label="电话号码" width="150"></el-table-column>
      <el-table-column prop="integral" label="积分" width="100"></el-table-column>
      <el-table-column prop="money" label="余额" width="100"></el-table-column>
      <el-table-column prop="payType" label="支付类型" width="80"></el-table-column>
      <el-table-column prop="address" label="地址" width="220"></el-table-column>
      <el-table-column label="操作">
      <template slot-scope="scope">
        <el-button
          size="mini"
          @click="handleEdit(scope.$index, scope.row)">编辑</el-button>
        <el-button
          size="mini"
          type="danger"
          @click="handleDelete(scope.$index, scope.row)">删除</el-button>
      </template>
    </el-table-column>
    </el-table>
  </div>
</template>
```
### 过滤器实现数据转换
>需求

将payType数据1，2，3，4转换为微信，支付宝，信用卡，现金
>代码实现

编辑src/views/member/index.vue
```
<script>
const payTypeOptions = [
  {type:'1',name:'现金'},
  {type:'2',name:'微信'},
  {type:'3',name:'支付宝'},
  {type:'4',name:'信用卡'}
]
export default {

    filters:{
    //filters中的this指的不是vue实例，而实无法直接获取data中的数据
    payTypeFilter(type){
      const obj = payTypeOptions.find(obj=>obj.type==type)
      //返回非空类型名称
      return obj ? obj.name:null
    }
  }
}
</script>
```
编辑模板
```
<el-table-column prop="payType" label="支付类型" width="80">
        <template slot-scope="scope">
          <span>{{ scope.row.payType | payTypeFilter }}</span>
        </template>
      </el-table-column>
```
### 数据分页
>需求

为列表添加分页功能
>API 接口约束

|名称|描述|
|:---|:---|
|请求URL|members/list/search/{page}/{size}|
|请求方式|post|

数据格式
```
{
    "status": "true",
    "message": "会员列表查询成功",
    "data": [
        {
            "id": 20,
            "cardNum": 6253976770739017,
            "name": "蔡勇",
            "birthday": "2006-02-23",
            "phone": 15146388481,
            "integral": "64.00",
            "money": "518.84",
            "payType": 2,
            "address": "新疆维吾尔自治区 阿克苏地区 沙雅县"
        },...
```
>vue接口

在src/api/member.js中默认的导出方法中添加search方法
```
import request from "@/utils/request";
export default {
    search(page,size){
        return request({
            url:`api/members/list/search/${page}/${size}`,
            method: 'post'
        })
    }

}
```
>src/views/member/index.vue调用search方法

在member js中将使用search方法将数据进行分页处理
```
export default {
  data(){
    return {
      list:[],
      total:0,  //数据总数
      currentPage:1, //当前页,默认第一页
      pageSize:10,  //页面条数
      currentPage4: 1
    }
  },
  //钩子函数获取数据
  created() {
    this.fetchData()
  },
  methods:{
    fetchData(){
      // getMemberList.getList().then(response=>{
      //   this.list = response.data.data
      // })
      getMemberList.search(this.currentPage,this.pageSize).then(response =>{
        console.log('response',response.data)
        const res = response.data
        console.log('res',res)
        this.total = res.data.total
        this.list = res.data.rows
        console.log('total',this.total,this.list)
      })
    }
}
```
>分页模板

```
    <el-pagination
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
      :current-page="currentPage4"
      :page-sizes="[10, 20, 30, 40]"
      :page-size="pageSize"
      layout="total, sizes, prev, pager, next, jumper"
      :total="total">
    </el-pagination>
```
参数
|参数|描述|
|:---|:---|
|size-change|分页显示每页数据条数，通过选择page-sizes触发函数|
|current-change||
|current-page|当前页默认页数，默认第一页|
|page-sizes|可选每页显示条数|
|layout|当前分页布局|
>分页功能实现

```
export default {
  data(){
    return {
      list:[],
      total:0,  //数据总数
      currentPage:1, //当前页,默认第一页
      pageSize:10,  //页面条数
      currentPage4: 4
    }
  },
  //钩子函数获取数据
  created() {
    this.fetchData()
  },
  methods:{
    fetchData(){
      // getMemberList.getList().then(response=>{
      //   this.list = response.data.data
      // })
      getMemberList.search(this.currentPage,this.pageSize).then(response =>{
        console.log('response',response.data)
        const res = response.data
        console.log('res',res)
        this.total = res.data.total
        this.list = res.data.rows
        console.log('total',this.total,this.list)
      })
    },
    handleEdit(id){
      console.log('edit',id)
    },
    handleDelete(id){
      console.log('delete',id)
    },
    handleSizeChange(val){
      console.log(`当前页: ${val} 条`)
      //将每页显示条数设置为分页选择的数量比如10，20，30，40
      this.pageSize = val
      //刷新页面数据
      this.fetchData()
    },
    handleCurrentChange(val){
      console.log(`当前页： ${val}`)
      //根据分页器选择页面跳转到对应页面比如选择页面2，跳转到第二个页面
      this.currentPage = val
      this.fetchData()
    }

  }
}
```
### 条件查询
>条件查询模板

```
<!--    margin 上 右 下 左-->
    <el-form :inline="true" :model="searchMap" class="demo-form-inline" style="margin: 10px 10px -10px 5px">
      <el-form-item>
        <el-input v-model="searchMap.carNum" placeholder="卡号"></el-input>
      </el-form-item>
            <el-form-item>
        <el-input v-model="searchMap.name" placeholder="姓名"></el-input>
      </el-form-item>
      <el-form-item>
        <el-select v-model="searchMap.payType" placeholder="支付类型">
          <el-option v-for="option in payTypeOptions" :key="option.type" :label="option.name" :value="option.type"></el-option>
        </el-select>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="fetchData">查询</el-button>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">重置</el-button>
      </el-form-item>
    </el-form>
```
>支付类型显示数据

```
export default {
  data(){
    return {
      list:[],
      total:0,  //数据总数
      currentPage:1, //当前页,默认第一页
      pageSize:10,  //页面条数
      currentPage4: 1, //默认页面，比如默认选择第一页
    //  条件查询重置
      searchMap:{
        carNum:'',
        name:'',
        payType:''
      },
      payTypeOptions, //等同于payTypeOptions=payTypeOptions
    }
  }
}
```
>定义查询接口

```
import request from "@/utils/request";
export default {
    search(page,size,searchMap){
        return request({
            url:`api/members/list/search/${page}/${size}`,
            method: 'post',
            data: searchMap
        })
    }

}
```
>js调用searchMap

```
export default {
  data(){
    return {
      list:[],
      total:0,  //数据总数
      currentPage:1, //当前页,默认第一页
      pageSize:10,  //页面条数
      currentPage4: 1, //默认页面，比如默认选择第一页
    //  条件查询重置
      searchMap:{
        cardNum:'',
        name:'',
        payType:''
      },
      payTypeOptions, //等同于payTypeOptions=payTypeOptions
    }
  },
  //钩子函数获取数据
  created() {
    this.fetchData()
  },
  methods:{
    fetchData(){
      // getMemberList.getList().then(response=>{
      //   this.list = response.data.data
      // })
      getMemberList.search(this.currentPage,this.pageSize,this.searchMap).then(response =>{
        //此时向api发送数据会携带{cardNum: "", name: "", payType: "3"}数据；api接口可通过携带的data数据对后台数据进行过滤
        console.log('response',response)
        const res = response.data
        this.total = res.data.total
        this.list = res.data.rows
      })
    },

  },
}

```
django 后端数据过滤实现
```
class MemberSearch(View):
    def post(self,request,page,size):
        import json
        res_data = json.loads(request.body)
        # 判断前端传递过来的data字典的value值是否为空，如果为空则删除
        for key in list(res_data.keys()):
            if not res_data.get(key):
                del res_data[key]
        print(res_data)
        member_obj = Member.objects.filter(**res_data)
```
### 搜索表单重置功能
> vue template

```
<!--    margin 上 右 下 左-->
    <el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline" style="margin: 10px 10px -10px 5px">
      <el-form-item prop="cardNum">
        <el-input v-model="searchMap.cardNum" placeholder="卡号"></el-input>
      </el-form-item>
      <el-form-item prop="name">
        <el-input v-model="searchMap.name" placeholder="姓名"></el-input>
      </el-form-item>
      <el-form-item prop="payType">
        <el-select v-model="searchMap.payType" placeholder="支付类型">
          <el-option v-for="option in payTypeOptions" :key="option.type" :label="option.name" :value="option.type"></el-option>
        </el-select>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="fetchData">查询</el-button>
        <el-button @click="resetForm('searchForm')">重置</el-button>
      </el-form-item>
    </el-form>
```
> vue js函数

```
resetForm(formName){
  //想要reset表单需要在el-form-item标签指定prop属性指定字段名称
  console.log('reset',formName)
  this.$refs[formName].resetFields()
}
```
## 增加数据
### 增加数据模板
```
<!--    增加会员对话框-->
<el-dialog title="会员增加" :visible.sync="dialogFormVisible" width="500px">
  <el-form ref="jopoForm"
           label-width="100px"
           label-position="right"
           style="width: 400px"
           :model="jopo">
    <el-form-item label="会员名称">
      <el-input v-model="jopo.name" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="会员生日" >
      <el-date-picker
          v-model="jopo.birthday"
          type="date"
          placeholder="选择日期">
      </el-date-picker>
    </el-form-item>
    <el-form-item label="积分">
      <el-input v-model="jopo.integral" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="金额">
      <el-input v-model="jopo.money" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="电话号码">
      <el-input v-model="jopo.phone" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="支付类型">
      <el-select v-model="jopo.payType" placeholder="请选择活动区域">
        <el-option v-for="option in payTypeOptions" :key="option.type" :label="option.name"
                   :value="option.type"></el-option>
      </el-select>
    </el-form-item>
    <el-form-item label="地址">
      <el-input type="textarea" autosize v-model="jopo.address" autocomplete="off"></el-input>
    </el-form-item>
  </el-form>
  <div slot="footer" class="dialog-footer">
    <el-button @click="dialogFormVisible = false">取 消</el-button>
    <el-button type="primary" @click="dialogFormVisible = false">确 定</el-button>
  </div>
</el-dialog>
```
>data中声明jopo和dialogFormVisible属性

```
export default {
  data(){
    return {
    //  增加数据对话框
      dialogFormVisible:false, //设置为false隐藏对话框，true显示对话框
      jopo:{
        integral:0,
        money:0,
        phone:13811111111
      },
```
>methods中添加提交表单函数，点击确定提交表单

```
methods:{
    addData(formName){
      this.$refs[formName].validate(valid =>{
        if(valid){
        //  校验通过
          console.log('add submit')
        }else {
          return false
        }
      })
    }
  }
```
>搜索框新增添加按钮

```
  <el-form-item>
    <el-button type="primary" @click="fetchData">查询</el-button>
    <el-button type="primary" @click="add('jopoForm')">增加</el-button>
    <el-button @click="resetForm('searchForm')">重置</el-button>
  </el-form-item>
```
>methods中添加add函数，用来增加按钮调用弹出框

```
add(formName){
  console.log('对话框',formName)
  this.dialogFormVisible = true
  this.$nextTick(() =>{
    //this.$nextTick是一个异步事件，当渲染结束后，它的回调函数才会执行
    //窗口弹出之后需要加载Dom，我们需要等Dom加载完成之后再调用resetFields函数重置表单
    this.$refs['jopoForm'].resetFields()
  })

}
```
### 表单数据校验
>在对话框定义rule标签

```
<el-form ref="jopoForm" :rules="rules">
```
>需要数据校验的表单设置prop属性

```
<el-form-item label="会员名称" prop="name">
  <el-input v-model="jopo.name" autocomplete="off"></el-input>
</el-form-item>
<el-form-item label="支付类型" prop="payType">
  <el-select v-model="jopo.payType" placeholder="请选择活动区域">
    <el-option v-for="option in payTypeOptions" :key="option.type" :label="option.name"
               :value="option.type"></el-option>
  </el-select>
</el-form-item>
```
>定义表单校验规则

```
  data(){
    return {
    //  增加数据表单数据校验
      rules:{ //数据校验需要在表单el-form-item设置prop属性
        name:[{required:true,message:'姓名不能为空',trigger:'blur'}],
        payType:[{required:true,message:'支付类型不能为空',trigger:'change'}]
      }
    }
  }
```
### 表单数据提交
>增加会员数据接口

|名称|描述|
|:---|:---|
|URL|member/add|
|请求方式|post|

示例
```
{
    status:'true',
    message: '新增成功'
}
```
>api调用接口

在src/api/member.js中定义添加数据接口
```
import request from "@/utils/request";
export default {
    memberAdd(jopo){
        return request({
            url: 'member/add',
            method: 'post',
            data: jopo
        })
    }

}
```
>表单提交数据

在src/views/member/index.vue
```
import Member from '@/api/member'
export default {
  data(){
    return {
        ......
    }
  },
  //钩子函数获取数据
  created() {
    this.fetchData()
  },
  methods:{
    addData(formName){
      this.$refs[formName].validate(valid =>{
        if(valid){
        //  校验通过
          console.log('add submit')
          Member.memberAdd(this.jopo).then(response =>{
            const resp = response.data
            console.log('resp',resp)
            if (resp.status){
              this.fetchData() //刷新数据
              this.dialogFormVisible = false //关闭窗口
            }else {
              this.$message({
                message: resp.message,
                type: 'warning'
              })
            }
          })
        }else {
          return false
        }
      })
    }

  }
}
```
## 修改数据
### API 接口
修改数据涉及两个接口查询接口、更新接口;点击编辑按钮打开编辑对话框，点击编辑对话框确认按钮发送更新数据接口
>查询接口

|说明|描述|
|:---|:---|
|URL|member/{id}|
|请求方法|get|

>更新接口

|说明|描述|
|:---|:---|
|URL|member/{id}|
|请求方法|put|

>vue请求接口

```
import request from "@/utils/request";
export default {
    get(id){ //获取单条数据接口
        return request({
            url: `api/member/${id}`,
            method: 'get'
        })
    },
    modify(jopo){
        return request({
            url: `api/member/${jopo.id}`,
            method: 'put',
            data: jopo
        })
    }

}
```
在data中定义id=null的属性，当id为null时对话框为编辑，否则为新增
```
export default {
  data(){
    return {
      jopo:{
        id: null,
        name:'', //不定义name字段，无法输入数据
        integral:0,
        money:0,
        phone:13811111111
      }
    }
  }
}
```
### template模板修改
编辑对话框确认按钮
```
<el-button type="primary" @click="jopo.id == null ? addData('jopoForm'):modify_member('jopoForm')">确 定</el-button>
```
### 重写js函数
edit函数，用来获取数据并打开对话框窗口
```
handleEdit(id){
  console.log('edit',id)

  Member.get(id).then(response =>{
    const resp = response.data
    console.log('edit',resp)
    this.dialogFormVisible = true //获取到数据之后打开编辑窗口
    this.jopo = resp.data //将查询到的数据赋值给表单对应的value
    console.log('jopo',this.jopo)
  })
}
```
modify_member函数，用来校验修改的数据并提交数据
```
modify_member(formName){
      console.log('确认修改',formName)
      this.$refs[formName].validate(valid =>{
        if(valid){
          //校验通过，向api发送请求
          console.log('jopo data',this.jopo)
          Member.modify(this.jopo).then(response =>{
            const resp = response.data
            console.log('modify resp',resp)
            if (resp.status){
              //数据更新成功关闭窗口并刷新数据
              this.dialogFormVisible = false
              this.fetchData()
            }
          })
        }else {
          //校验不通过
        }
      })
    },
```
## 删除会员
### API请求接口
```
delete(id){
    return request({
        url:`api/member/${id}`,
        method: 'delete'
    })
}
```
### 删除数据button
```
<el-button size="mini" type="danger" @click="handleDelete(scope.$index, scope.row.id)">删除</el-button>
```
### 删除数据函数
```
handleDelete(id){
      console.log('delete',id)
      Member.delete(id).then(response =>{
        const resp = response.data
        console.log('delete resp',resp)
        if (resp.status){
          //如果后端api接口返回true则刷新数据
          this.fetchData()
        }
      })
    }
```
# 供应商管理
## 供应商表格查询
### 供应商table模板
```
<template>
  <div>
    <el-table :data="supplierData" border style="width: 100%">
      <el-table-column prop="id" label="ID" width="180"></el-table-column>
      <el-table-column prop="name" label="供应商名称" width="180"></el-table-column>
      <el-table-column prop="linkman" label="联系人" width="180"></el-table-column>
      <el-table-column prop="mobile" label="联系电话" width="180"></el-table-column>
      <el-table-column prop="remark" label="描述" width="300"></el-table-column>
      <el-table-column label="操作">
      <template slot-scope="scope">
<!--        scope.row.id 获取行数据id-->
        <el-button size="mini" @click="handleEdit(scope.row.id)">编辑</el-button>
        <el-button size="mini" type="danger" @click="handleDelete(scope.row.id)">删除</el-button>
      </template>
    </el-table-column>
    </el-table>
  </div>
</template>
<style>
.el-table{
  /*上 右 下 左*/
  margin: 10px 5px 5px 10px;
}
</style>
```
### 表格请求接口
```
import request from '@/utils/request'

export default {
    list(){
        return request({
            url:'/api/supplier/',
            method:'get'
        })
    }
}
```
### 表格js
```
<script>
import supplierAPI from '@/api/supplier'
export default {
  data(){
    return {
      supplierData:[]
    }
  },
  //钩子函数用来获取数据
  created() {
    this.fetchData()
  },
  methods:{
    fetchData(){
      supplierAPI.list().then(response =>{
        console.log('response',response.data)
        const resp = response.data
        this.supplierData = resp.data
      })
    },
    handleEdit(id){
      console.log('编辑',id)
    },
    handleDelete(id){
      console.log('删除',id)
    }
  }
}
</script>
```
## 供应商分页
### 请求接口
编辑src/api/supplier.js
```
import request from '@/utils/request'

export default {
    search(page,size,searchMap){
        return request({
            url:`/api/supplier/search?page=${page}&size=${size}`,
            method: 'post',
            data: searchMap
        })
    }
}
```
### 分页模板
```
<!--    分页-->
    <el-pagination 
        @size-change="handleSizeChange" 
        @current-change="handleCurrentChange"
        :current-page="currentPage" 
        :page-sizes="[8, 16, 24, 32]" 
        :page-size="this.size"
        layout="total, sizes, prev, pager, next, jumper" 
        :total="this.total">
    </el-pagination>
```
### js
编辑src/views/supplier/index.vue
```
<script>
import supplierAPI from '@/api/supplier'

export default {
  data() {
    return {
      supplierData: [],
      searchForm: {
      },
      currentPage:1,
      total:0,
      page:1,
      size:8,
      searchMap:{
        name:'',
        linkman:'',
        mobile:''
      }
    }
  },
  //钩子函数用来获取数据
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
      supplierAPI.search(this.page,this.size,this.searchMap).then(response =>{
        console.log('size',this.size)
        console.log('response',response.data)
        const resp = response.data
        this.supplierData = resp.data
        this.total = resp.total

      })
    },
    handleEdit(id) {
      console.log('编辑', id)
    },
    handleDelete(id) {
      console.log('删除', id)
    },
    addData(){
      console.log('add')
    },
    reset(){
      console.log('reset')
    },
    handleSizeChange(size){
      this.size = size
      this.fetchData()
    },
    handleCurrentChange(page){
      console.log('test1',page)
      this.page = page
      this.fetchData()
    }
  }
}
</script>
```
## 供应商条件查询
### 条件查询模板
编辑src/views/supplier/index.vue
```
<!--    搜索栏-->
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline">
  <el-form-item>
    <el-input prop="name" v-model="searchMap.name" placeholder="供应商"></el-input>
  </el-form-item>
  <el-form-item>
    <el-input prop="linkman" v-model="searchMap.linkman" placeholder="联系人"></el-input>
  </el-form-item>
  <el-form-item>
    <el-input prop="mobile" v-model="searchMap.mobile" placeholder="联系电话"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="fetchData">查询</el-button>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="addData">新增</el-button>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="reset">重置</el-button>
  </el-form-item>
</el-form>
```
### js和请求接口
由于请求接口和list列表公用search方法，所以不用再写
## 重置搜索栏
搜索form模板设置prop属性到el-form-item标签、reset按钮传递form名称
### 重置搜索栏模板
```
<!--    搜索栏-->
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline">
  <el-form-item prop="name">
    <el-input  v-model="searchMap.name" placeholder="供应商"></el-input>
  </el-form-item>
  <el-form-item prop="linkman">
    <el-input  v-model="searchMap.linkman" placeholder="联系人"></el-input>
  </el-form-item>
  <el-form-item prop="mobile">
    <el-input  v-model="searchMap.mobile" placeholder="联系电话"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="fetchData">查询</el-button>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="addData">新增</el-button>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="reset('searchForm')">重置</el-button>
  </el-form-item>
</el-form>
```
### reset js函数
```
reset(formName){
  console.log('reset',formName)
  this.$refs[formName].resetFields()
},
```
## 供应商增加数据
### 供应商增加数据表单template
```
<!--    新增表单-->
<el-dialog width="30%" title="供应商编辑" :visible.sync="dialogFormVisible">
  <el-form
      ref="addForm"
      style="width: 82%"
      :label-width="formLabelWidth"
      :model="addFormData">
    <el-form-item label="供应商名称">
      <el-input  v-model="addFormData.name" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="联系人">
      <el-input v-model="addFormData.linkman" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="联系电话">
      <el-input v-model="addFormData.mobile" autocomplete="off"></el-input>
    </el-form-item>
    <el-form-item label="备注">
      <el-input v-model="addFormData.remark" autocomplete="off"></el-input>
    </el-form-item>
  </el-form>
  <div slot="footer" class="dialog-footer">
    <el-button @click="dialogFormVisible = false">取 消</el-button>
    <el-button type="primary" @click="subimitForm('addForm')">确 定</el-button>
  </div>
</el-dialog>
```
### 请求API
```
import request from '@/utils/request'

export default {
    add(addMap){
        return request({
            url:'/api/supplier/add',
            method:'post',
            data: addMap
        })
    }
}
```
### js
```
<script>
import supplierAPI from '@/api/supplier'

export default {
  data() {
    return {
      dialogFormVisible: false,
      addFormData:{},
      formLabelWidth: '82px'
    }
  },
  methods: {
    fetchData() {
        ......
      })
    },
    addData(){
      console.log('add')
      this.dialogFormVisible = true
    },
    subimitForm(formName){
      console.log('formName',formName)
      supplierAPI.add(this.addFormData).then(response =>{
        const resp = response.data
        console.log('resp',resp)
        if(resp.status){
          this.dialogFormVisible = false
          this.fetchData()
        }
      })

    }
  }
}
</script>
```
### 添加数据表单数据校验
模板添加rules属性
```
<el-form
  ref="addForm"
  :rules="addRule"
  style="width: 80%;"
  :label-width="formLabelWidth"
  :model="addFormData">
</el-form>
```
js添加rules规则
```
<script>
import supplierAPI from '@/api/supplier'

export default {
  data() {
    return {
      addRule:{
        name:[
          {required:true,message:'请输入供应商名称',trigger:'blur'}
        ],
        mobile:[
          {required:true,message:'请输入联系人电话',trigger:'blur'}
        ]
      }
    }
  },
}
</script>
```
## 供应商编辑
### 请求API接口
编辑功能涉及两个api接口，获取一条数据和更新一条数据
```
import request from '@/utils/request'

export default {
    get(id){
        return request({
            url:`/api/supplier/${id}`,
            method:'get'
        })
    },
    update(id,formName,formData){
        return request({
            url:`/api/supplier/${id}`,
            method:'put',
            data: formData
        })
    }
}
```
### 供应商编辑模板
供应商编辑模板表单个新增供应商表单共用一个表单，通过判断id为null的方式判断新增还是更新然后调用不同的函数
```
<el-button type="primary" @click="addFormData.id == null ? subimitForm('addForm'):updateForm('addForm')">确 定</el-button>
```
### js
```
<script>
import supplierAPI from '@/api/supplier'

export default {
  data() {
    return {
        ......
    }
  },
  //钩子函数用来获取数据
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
        ......
    },
    handleEdit(id) {
      console.log('编辑', id)
      supplierAPI.get(id).then(response =>{
        const resp = response.data
        console.log('resp',resp)
        if (resp.status){
          this.dialogFormVisible = true
          this.addFormData = resp.data
          console.log('data',this.addFormData)
        }
      })
    },
    subimitForm(formName){
        ......
    },
    updateForm(formName){
      //点击编辑按钮时已经把id赋值给addFormData对象，所以直接取出来就行
      supplierAPI.update(this.addFormData.id,formName,this.addFormData).then(response =>{
        const resp = response.data
        console.log('resp',resp)
        if (resp.status){
          this.dialogFormVisible = false
          this.fetchData()
        }
      })
    }
  }
}
</script>
```
## 供应商删除
删除功能不需要模板
### 请求API接口
```
import request from '@/utils/request'

export default {
    delete(id){
        return request({
            url:`/api/supplier/${id}`,
            method:'delete'
        })
    }
}
```
### js函数
```
handleDelete(id) {
  console.log('删除', id)
  supplierAPI.delete(id).then(response =>{
    const resp = response.data
    console.log('resp',resp)
    if(resp.status){
      this.fetchData()
    }
  })
}
```
# 商品管理
### 商品列表
#### template
```
<!--    table表格-->
<!--    data属性要显示的数据，数据类型为array-->
<el-table :data="googList"  border style="width: 100%">
  <el-table-column prop="index" type="index" :index="goodIndex" label="ID" width="60"></el-table-column>
  <el-table-column prop="name" label="名称" width="160"></el-table-column>
  <el-table-column prop="code" label="代码" width="180"></el-table-column>
  <el-table-column prop="retailPrice" label="零售价" width="180"></el-table-column>
  <el-table-column prop="purchasePrice" label="出售价" width="180"></el-table-column>
  <el-table-column prop="supplier" label="供应商" width="180"></el-table-column>
  <el-table-column prop="spec" label="具体" width="180"></el-table-column>
  <el-table-column prop="storageNum" label="库存量" width="180"></el-table-column>
</el-table>
```
#### js
```
<script>
import goodAPI from "@/api/goods"

export default {
  data() {
    return {
      googList: []
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
      console.log('fetch data')
      this.formSearch()
    },
    formSearch() {
      console.log('form submit')
      goodAPI.search(this.page, this.size, this.searchMap).then(response => {
        const resp = response.data
        console.log('form search resp', resp)
        this.total = resp.data.kwargs.count
        if (resp.code == 200){
          this.googList = resp.data.data
        }
      })
    },
    goodIndex(index){
      console.log('index',index)
      return index + 1
    }
  }
}
</script>
```
### 商品分页
#### template
```
<el-pagination
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
      :current-page="currentPage4"
      :page-sizes="[8, 16, 24, 32]"
      :page-size="100"
      layout="total, sizes, prev, pager, next, jumper"
      :total="this.total">
    </el-pagination>
```
#### js
```
<script>
import goodAPI from "@/api/goods"

export default {
  data() {
    return {
      page: 1,
      size: 8,
      total:0,
      currentPage4:1,
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
      console.log('fetch data')
      this.formSearch()
    },
    formSearch() {
      console.log('form submit')
      goodAPI.search(this.page, this.size, this.searchMap).then(response => {
        const resp = response.data
        console.log('form search resp', resp)
        this.total = resp.data.kwargs.count
        if (resp.code == 200){
          this.googList = resp.data.data
        }
      })
    },
    handleSizeChange(val){
      //选择每页显示多少条
      console.log(val)
      this.size = val
    //  刷新页面
      this.fetchData()
    },
    handleCurrentChange(val){
       //根据选择页码刷新数据
      console.log(val)
      this.page = val
      this.fetchData()
    }
  }
}
</script>
```
### 商品查询
#### template
```
<!--    搜索栏-->
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline">
  <el-form-item>
    <el-input v-model="searchMap.name" placeholder="商品名称"></el-input>
  </el-form-item>
  <el-form-item>
    <el-input v-model="searchMap.code" placeholder="代码"></el-input>
  </el-form-item>
  <el-form-item>
    <el-input v-model="searchMap.supplier" placeholder="供应商"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="formSearch()">查询</el-button>
    <el-button type="primary" @click="formAdd">新增</el-button>
    <el-button @click="formReset">重置</el-button>
  </el-form-item>
</el-form>
```
#### js
```
<script>
import goodAPI from "@/api/goods"

export default {
  data() {
    return {
      searchMap: {
        name: '',
        code: '',
        supplier: ''
      },
      googList: []
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
      console.log('fetch data')
      this.formSearch()
    },
    formSearch() {
      console.log('form submit')
      goodAPI.search(this.page, this.size, this.searchMap).then(response => {
        const resp = response.data
        console.log('form search resp', resp)
        this.total = resp.data.kwargs.count
        if (resp.code == 200){
          this.googList = resp.data.data
        }
      })
    }
  }
}
</script>
```
#### 使用filter将表格中供应商ID转换为供应商名称

## 搜索栏功能
### 重置
#### template
```
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline">
      <el-form-item prop="name">
        <el-input  v-model="searchMap.name" placeholder="商品名称"></el-input>
      </el-form-item>
      <el-form-item prop="code">
        <el-input v-model="searchMap.code" placeholder="代码"></el-input>
      </el-form-item>
      <el-form-item prop="supplier">
        <el-input v-model="searchMap.supplier" placeholder="供应商"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button @click="formReset('searchForm')">重置</el-button>
      </el-form-item>
    </el-form>
```
#### js
```
formReset(formName) {
  console.log('form reset')
  this.$refs[formName].resetFields()
},
```
### 新增
### 删除
#### template
```
<template slot-scope="scope">
<!--          scope.row是当前行对象-->
<el-button
  size="mini"

  @click="handleEdit(scope.row.id)">编辑</el-button>
<el-button
  size="mini"
  type="danger"
  @click="handleDelete(scope.row.id)">删除</el-button>
</template>
```
#### js
```
handleDelete(index){
      goodAPI.delete(index).then(response =>{
        const resp = response.data
        if (resp.code == 200 ){
          this.$notify({type:"success",message:resp.data.msg})
          this.fetchData()
        }
        else {
          this.$notify({type:"error",message:resp.data.msg})
        }
      })
    }
```
### 供应商表单弹出供应商对话框
>实现效果

点击输入框，弹出一个对话框，对话框里直接复用供应商管理组件
>实现步骤

将supplier/index.vue导入到goods/index.vue将supplier作为goods的组件使用
```
<scirpt>
import supplier from "@/views/supplier"
export default {
    components: {supplier},
}
</script>
```
使用dialog组件将supplier包裹进去
```
<!--    supplier子组件-->
<el-dialog title="选择供应商" :visible="dialogsupplierVisible" width="500px">
  <supplier></supplier>
</el-dialog>
```
goods views中data定义dialogsupplierVisible属性
```
<script>
export default {
    data(){
        dialogsupplierVisible: false
    }
}
</script>
```
设置供应商对话框只读和点击事件
```      
<el-form-item prop="supplier">
<!--readonly设置input标签为只读，@click.native点击事件，点击之后显示dialog对话框-->
    <el-input readonly @click.native="dialogsupplierVisible = true" v-model="searchMap.supplier" placeholder="供应商"></el-input>
  </el-form-item>
```
供应商对话框页面优化，通过父组件向子组件传递数据的方式，通过props声明接收向supplier子组件传递isDialog属性。如果是弹出对话框则隐藏一些不必要的属性
```
<el-dialog title="选择供应商" :visible="dialogsupplierVisible" width="500px">
  <supplier :isDialog="true"></supplier>
</el-dialog>
```
在supplier/index.vue中使用props接收isDialog属性
```
<script>
export default {
  props:{
    isDialog:Boolean
  },
}
</script>
```
在子组件supplier中table列表页面中使用v-if判断是否隐藏
```
#搜索栏
  <el-form-item prop="mobile">
    <el-input v-if="!isDialog"  v-model="searchMap.mobile" placeholder="联系电话"></el-input>
  </el-form-item>
```
```
#table列表
<el-table-column v-if="!isDialog" prop="mobile" label="联系电话" width="180"></el-table-column>
```
```
#分页
<el-pagination
    @size-change="handleSizeChange"
    @current-change="handleCurrentChange"
    :current-page="currentPage"
    :page-sizes="[8, 16, 24, 32]"
    :page-size="this.size"
    :layout="!isDialog ? 'total, sizes, prev, pager, next, jumper': 'prev, pager, next'"
    :total="this.total">
</el-pagination>
```
当点击子组件supplier供应商一行数据时，返回到父组件goods商品。在table列表绑定current-chanage事件,编辑supplier/index.vue
```
<!--    highlight-current-row实现单行可选；current-change-row 通过current-change事件管理选中时触发的事件函数clickCurrentChange并传入currentRow，oldCurrentRow参数-->
<el-table :highlight-current-row="isDialog" @current-change="clickCurrentChange" :data="googList" border style="width: 100%">
</el-table>
```
js
```
clickCurrentChange(currentRow){
  console.log('currentRow',currentRow)
  //currentRow返回选择一行数据的对象
  this.$emit('option-supplier',currentRow)
}
```
在父组件goods/index.vue中supplier绑定自定义事件option-supplier
```
<el-dialog title="选择供应商" :visible="dialogsupplierVisible" width="600px">
  <supplier @option-supplier="optionSupplier" :isDialog="true"></supplier>
</el-dialog>
```
在父组件goods/index.vue中添加optionSupplier函数，进行数据回显
```
optionSupplier(currentRow){
  console.log('goods currentRow',currentRow)
  this.searchMap.supplier = currentRow.name
  this.dialogsupplierVisible = false
}
```
### 新增商品
>需求

点击新增按钮打开新增对话框，点击供应商打开供应商列表选择后返回供应商

template
```
  <el-form-item>
    <el-button type="primary" @click="formAdd">新增</el-button>
  </el-form-item>
```
js
```
formAdd() {
  console.log('form add')
  this.dialogEditVisible = true
},
```
新增对话框的供应商表单绑定自定义点击事件,点击之后打开供应商列表
```
<el-form-item label="供应商">
  <el-input @click.native="dialogsupplierVisible = true" v-model="goodObj.supplier" autocomplete="off"></el-input>
</el-form-item>
```
编辑optionSupplier函数，将选择的供应商名称返回给新增表单的供应商input标签
```
    optionSupplier(currentRow){
      console.log('goods currentRow',currentRow)
      this.goodObj.supplier = currentRow.name
    }
```
点击新增表单确认按钮发送数据到api
```
EditConfirm(){
      console.log('confirm',this.goodObj)
      //当标签填入数据之后goodObj对象就是填入的数据对象
      goodAPI.add(this.goodObj).then(response =>{
        const resp = response.data
        console.log('resp',resp)
        if (resp.code == 200){
          this.dialogEditVisible = false
          this.$message.success({message:resp.data.msg})
        }else {
          this.$message.error({message:resp.data.msg})
        }
      })

    },
```