基于vue-admin-template 4.4.0版本二次开发
# 项目环境搭建
>软件版本

|软件名称｜版本号｜
|:---|:---|
|nodejs|v12.16.1|
|npm|6.13.4|
## 基础配置
>代码下载

```
git clone https://github.com/PanJiaChen/vue-admin-template.git
```
>汉化

编辑src/main.js
```
// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)
```
>关闭eslint语法校验

编辑vue.config.js
```
lintOnSave: false
```
>设置title

编辑src/settings.js
```
module.exports = {

  title: 'Blog manage platform', // 设置title
  fixedHeader: true, // 固定头部
  sidebarLogo: true // 显示图标
}
```
编辑store下settings模块文件，将src/settings.js中title的配置导入到store
```
#vi src/store/modules/settings.js
const { showSettings, fixedHeader, sidebarLogo, title } = defaultSettings

const state = {
  showSettings: showSettings,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  title: title
}
```
从src/layout/index.vue定位到src/layout/components/Sidebar/Logo.vue
```
data() {
    return {
      title: this.$store.state.settings.title,
      logo: 'https://wpimg.wallstcn.com/69a1c46c-eb1c-4b46-8bd4-e9e686ef5251.png'
    }
  }
```
>Django API接口配置

编辑.env.development
```
VUE_APP_BASE_API = '/['
```
编辑vue.config.js
```
devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    before: require('./mock/mock-server.js'),
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: 'http://127.0.0.1:8000',
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }
  }
```
# 菜单栏
## 路由配置
脚手架会根据路由配置来自动生成菜单栏，路由配置文件位于`src/router/index.js`
```
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },

  {
    path: '/blog',
    component: Layout,
    redirect: '/article',
    name: 'blog',
    meta: { title: '博客管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'article',
        name: 'Article',
        component: () => import('@/views/blog/article/index'),
        meta: { title: '文章管理', icon: 'table' }
      },
      {
        path: 'category',
        name: 'Category',
        component: () => import('@/views/blog/category/index'),
        meta: { title: '分类管理', icon: 'tree' }
      },
      {
        path: 'tags',
        name: 'Tags',
        component: () => import('@/views/blog/tags/index'),
        meta: { title: '标签管理', icon: 'tree' }
      }
    ]
  },

  {
    path: '/settings',
    component: Layout,
    redirect: '/settings/user',
    name: 'User',
    meta: {
      title: '系统设置',
      icon: 'nested'
    },
    children: [
      {
        path: 'user',
        component: () => import('@/views/settings/user/index'), // Parent router-view
        name: 'User',
        meta: { title: '用户管理' }
      },
      {
        path: 'menu',
        component: () => import('@/views/settings/menu/index'),
        meta: { title: '菜单管理' }
      }
    ]
  },
  {
    path: '/ad',
    component: Layout,
    redirect: '/ad/index',
    name: 'Ad',
    meta: {
      title: '广告管理',
      icon: 'nested'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/ad/index'),
        meta: { 'title': '广告管理' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
```
## 面包屑Dashboar中文显示
面包屑路径`src\components\Breadcrumb\index.vue`
```
getBreadcrumb() {
      // only show routes with meta.title
      let matched = this.$route.matched.filter(item => item.meta && item.meta.title)
      const first = matched[0]

      if (!this.isDashboard(first)) {
        matched = [{ path: '/dashboard', meta: { title: '首页' }}].concat(matched)
      }

      this.levelList = matched.filter(item => item.meta && item.meta.title && item.meta.breadcrumb !== false)
    }
```
# 博客管理
## 分类管理
### table列表页面数展示
编辑`src/views/blog/category/index.vue`
```
<template>
  <div>
    <el-table ref="listForm" :data="list" style="width: 100%" border>
      <!--    :data 绑定数据-->
      <el-table-column prop="id" label="ID" width="150"></el-table-column>
      <!--    prop id将数据id绑定-->
      <el-table-column prop="name" label="分类" width="300"></el-table-column>
      <el-table-column prop="status" label="状态" width="200">
        <template slot-scope="scope">
<!--          过滤器前面scope.row.status的值作为参数值传入statusOption等同于statusOption(scope.row.status)-->
          {{ scope.row.status | statusOption }}
        </template>
      </el-table-column>
      <el-table-column prop="isTop" label="是否置顶" width="200">
        <template slot-scope="scope">
          {{ scope.row.isTop | isTopOption }}
        </template>
      </el-table-column>
      <el-table-column prop="comment" label="备注" width="480"></el-table-column>
      <el-table-column label="操作">
        <template>
          <el-button size="mini" @click="handleEdit">编辑</el-button>
          <el-button size="mini" type="danger" @click="handleDelete">删除</el-button>
        </template>
      </el-table-column>
    </el-table>
  </div>
</template>
```
```
<script>
import api from '@/api/category'

export default {
  filters: {
    statusOption(status) {
      const statusMap = { 0: '发布', 1: '未发布' }
      return statusMap[status]
    },
    isTopOption(val) {
      const isTopMap = { 0: '是', 1: '否' }
      return isTopMap[val]
    }
  },
  data() {
    return {
      list: [],
      listLoading: true,
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      // eslint-disable-next-line no-unused-vars
      const response = await api.get()
      console.log('response', response)
      if (response.code === 20000) {
        this.listLoading = false
        this.list = response.data
      }
    }
  }
}
</script>
```
### table列表页面分页器
>template

```
<el-pagination
      v-if="this.total >= 10"
      @size-change="handleSizeChange"
      @current-change="handlePageChange"
      :current-page="currentPage"
      :page-sizes="[10, 20, 30, 40]"
      :page-size="10"
      layout="total, sizes, prev, pager, next, jumper"
      :total="this.total">
    </el-pagination>
```
>js

```
<script>
import api from '@/api/category'

export default {
  data() {
    return {
      list: [],
      listLoading: true,
      page: 1,
      size: 10,
      searchMap: {},
      currentPage: 1,
      total: 0
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      // eslint-disable-next-line no-unused-vars
      const response = await api.search(this.page, this.size, this.searchMap)
      console.log('response', response)
      if (response.code === 20000) {
        this.listLoading = false
        this.list = response.data.results
        this.total = response.data.count
      }
    },
    async handleSizeChange(size) {
      console.log('size', size)
      const response = await api.search(this.page, size, this.searchMap)
      this.list = response.data.results
    },
    async handlePageChange(page) {
      const response = await api.search(page, this.size, this.searchMap)
      this.list = response.data.results
    }
  }
}
</script>
```
### 搜索栏
>template

```
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline">
      <el-form-item prop="name">
        <el-input v-model="searchMap.name" placeholder="分类名称" size="small"></el-input>
      </el-form-item>
      <el-form-item prop="status">
        <el-select v-model="searchMap.status" placeholder="状态" size="small">
          <el-option v-for="item in statusOption" :label="item.value" :value="item.key" :key="item.key"></el-option>
        </el-select>
      </el-form-item>
      <el-form-item prop="isTop">
        <el-select v-model="searchMap.isTop" placeholder="是否置顶" size="small">
          <el-option v-for="item in isTopOption" :label="item.value" :value="item.key" :key="item.key"></el-option>
        </el-select>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="searchQuery" size="small">查询</el-button>
        <el-button @click="resetQuery('searchForm')" size="small">重置</el-button>
        <el-button type="primary" @click="add" size="small">增加</el-button>
      </el-form-item>
    </el-form>
```
>js

```
<script>
import api from '@/api/category'

export default {
  filters: {
    statusOption(status) {
      const statusMap = { 0: '发布', 1: '未发布' }
      return statusMap[status]
    },
    isTopOption(val) {
      const isTopMap = { 0: '是', 1: '否' }
      return isTopMap[val]
    }
  },
  data() {
    return {
      listLoading: true,
      list: [],
      statusOption: [{ key: 0, value: '发布' }, { key: 1, value: '未发布' }],
      isTopOption: [{ key: 0, value: '是' }, { key: 1, value: '否' }],
      searchMap: {
        name: '',
        status: '',
        isTop: ''
      },
      dialogFormVisible: false,
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const response = await api.search(this.page, this.size, this.searchMap)
      console.log('fetch data response', response)
      if (response.code === 20000) {
        this.listLoading = false
        this.list = response.data.results
        this.total = response.data.count
      }
    },
    searchQuery() {
      this.fetchData()
    },
    resetQuery(formName) {
      this.$refs[formName].resetFields()
    }
  }
}
</script>
```
## 标签管理
### 标签table列表
采用组件化方式将table列表拆分成子组件，编辑`src/views/blog/tags/index.vue`
```
<template>
  <div>
    <tableList></tableList>
  </div>
</template>
<script>
import tableList from '@/views/blog/tags/tableList'

export default {
  // eslint-disable-next-line no-undef
  components: { tableList }
}
</script>
```
编辑`src/views/blog/tags/tableList.vue`
```
<template>
<el-table :data="tableList" border style="width: 100%">
      <el-table-column type="index" label="序号" width="280"></el-table-column>
      <el-table-column prop="name" label="标签名称" width="400"></el-table-column>
      <el-table-column prop="category" label="分类名称" width="400"></el-table-column>
      <el-table-column label="操作" width="240">
        <template slot-scope="scope">
          <el-button size="small" @click="handleEdit(scope.row.id)">编辑</el-button>
          <el-button size="small" type="danger" @click="handleDelete(scope.row.id)">删除</el-button>
        </template>
      </el-table-column>
    </el-table>
</template>

<script>
import api from '@/api/tag'
export default {
  data() {
    return {
      tableList: []
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      console.log('test1')
      const response = await api.getList()
      console.log(response)
      this.tableList = response.data.results
    },
    async handleEdit(id) {
      const response = await api.get(id)
      console.log('test', response)
    },
    handleDelete(id) {
      console.log(id)
    }
  }
}
</script>
```
### 分页器
编辑`src/views/blog/tags/index.vue`将分页器组件抽取出来
```
<template>
  <div>
    <pagination @changeSize="changeSize" @changePage="changePage" :total="total"></pagination>
  </div>
</template>
```
```
<script>
import pagination from '@/views/blog/tags/pagination'
export default {
  // eslint-disable-next-line no-undef,vue/no-unused-components
  components: { tableList, search, pagination },
  data() {
    return {
      page: 1,
      size: 10,
      total: 0,
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const response = await api.getList(this.page, this.size, this.searchMap)
      console.log('response', response)
      this.list = response.data.results
      this.total = response.data.count
    },
    changeSize(data) {
      this.size = data
      this.fetchData()
    },
    changePage(data) {
      this.page = data
      this.fetchData()
    }
  }
}
</script>
```
编辑`src/views/blog/tags/pagination.vue`编写分页器子组件
```
<template>
  <el-pagination
    @size-change="changeSize"
    @current-change="changePage"
    :current-page="pageNumber"
    :page-sizes="[10, 20, 30, 40]"
    :page-size=this.size
    layout="total, sizes, prev, pager, next, jumper"
    :total=this.total>
  </el-pagination>
</template>
<script>
export default {
  props: {
    total: Number
  },
  data() {
    return {
      pageNumber: 1,
      size: 10
    }
  },
  methods: {
    changeSize(val) {
      this.size = val
      console.log('change size', val)
      // 子组件向父组件传递changesize
      this.$emit('changeSize', this.size)
      // 子组件调用父组件方法
      // this.$parent.fetchData()
    },
    changePage(val) {
      this.page = val
      this.$emit('changePage', this.page)
    }
  }
}
</script>
```
### 搜索框
子组件向父组件发送数据
>子组件

编辑`src/views/blog/tags/search.vue`
```
<template>
  <el-form :inline="true" :model="searchMap" class="demo-form-inline">
    <el-form-item>
      <el-input v-model="searchMap.name" placeholder="标签名称"></el-input>
    </el-form-item>
        <el-form-item>
      <el-input v-model="searchMap.category" placeholder="分类名称"></el-input>
    </el-form-item>
    <el-form-item>
      <el-button type="primary" @click="submitSearch" size="small">查询</el-button>
    </el-form-item>
  </el-form>
</template>
<script>
export default {
  data() {
    return {
      searchMap: {
        name: '',
        category: ''
      }
    }
  },
  methods: {
    submitSearch() {
      console.log('submit', this.searchMap)
      // $emit方法子组件向父组件传递数据，event参数子组件和父组件关联的事件名称submitSearch
      this.$emit('submitSearch', this.searchMap)
    }
  }
}
</script>
```
>父组件

编辑`src/views/blog/tags/index.vue`
```
<template>
  <div>
<!--    submitSearch子组件event的名称，submitData触发的函数-->
    <search @submitSearch="submitData"></search>
  </div>
</template>
<script>
import tableList from '@/views/blog/tags/tableList'
import search from '@/views/blog/tags/search'

import api from '@/api/tag'

export default {
  // eslint-disable-next-line no-undef,vue/no-unused-components
  components: { tableList, search },
  data() {
    return {
      searchMap: {
        name: '',
        category: ''
      }
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const response = await api.getList(this.page, this.size, this.searchMap)
      console.log('response', response)
      this.list = response.data.results
      this.total = response.data.count
    },
    submitData(data) {
      console.log('data', data)
      this.searchMap = data
      console.log('searchmap', this.searchMap)
      this.fetchData()
    }
  }
}
</script>
```
### 增加数据
>template

```
<el-button type="primary" @click="addData('editForm')" size="small">新增</el-button>
```
>js

```
addData(formName) {
      this.dialogFormVisible = true
    }
```
>dialog

```
<el-dialog  title="编辑标签" :visible.sync="dialogFormVisible" label-width="100px">
      <el-form :model="editMap" ref="editForm">
        <el-form-item label="标签名称">
          <el-input v-model="editMap.name" autocomplete="off" style="width: 200px"></el-input>
        </el-form-item>
        <el-form-item label="分类名称">
          <el-select v-model="editMap.category" filterable placeholder="请选择">
            <el-option
              v-for="item in list"
              :key="item.category"
              :label="item.category"
              :value="item.category">
            </el-option>
          </el-select>
        </el-form-item >
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button size="small" @click="dialogFormVisible = false">取 消</el-button>
        <el-button size="small" type="primary" @click="editMap.id == null ? submitEditData():submitUpdateData()">确 定</el-button>
      </div>
    </el-dialog>
```
```
addData(formName) {
  this.dialogFormVisible = true
}
```
## 文章管理
### 文章列表页面
将文章列表页面拆分未tableList组件，编辑`src/views/blog/article/index.vue`，获取列表数据
```
<template>
<!--  :list="list" 将父组件index.vue的list数据绑定-->
  <tableList :tableData="tableData"></tableList>
</template>
<script>
import api from '@/api/article'
import tableList from '@/views/blog/article/tableList'
export default {
  components: { tableList },
  data() {
    return {
      tableData: []
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const response = await api.getAll()
      console.log('response', response)
      this.tableData = response.data.results
    }
  }
}
</script>
```
编辑`src/views/blog/article/tableList.vue`子组件通过props属性接受父组件传递的数据，注意：父组件发送数据的list数据类型为数组，子组件接受数据的数据类型为数组
```
<template>
  <el-table :data="tableData" border style="width: 100%">
    <el-table-column type="index" label="序号" width="180"></el-table-column>
    <el-table-column prop="title" label="文章名称" width="180"></el-table-column>
    <el-table-column prop="status" label="状态" width="180"></el-table-column>
    <el-table-column prop="category" label="分类" width="180"></el-table-column>
    <el-table-column prop="tags" label="标签" width="180"></el-table-column>
    <el-table-column prop="updateDate" label="更新时间" width="180"></el-table-column>
    <el-table-column prop="createDate" label="创建时间" width="180"></el-table-column>
  </el-table>
</template>
<script>
export default {
  props: ['tableData']
}
</script>
```
### 分页
>注意

1. 父组件向子组件传值使用:xxx="xxx";
1. 子组件接受父组件用props属性
1. 父组件接受子组件传值使用@<event>=<function>
1. 子组件向父组件传值this.$emit('<event>', <data>) event是事件名称需要和父组件event保持一致，data是子组件向父组件传递的数据
编辑`src/views/blog/article/index.vue` template部分
```
<Pagination :total="total" @changeSize="changeSize" @changePage="changePage"></Pagination>
```
js部分
```
<script>
import Pagination from '@/views/blog/article/pagination'
export default {
  components: { Pagination },
  data() {
    return {
      total: 0,
      size: 10,
      page: 1,
      query: {}
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const { data } = await api.getAll(this.query, this.page, this.size)
      this.tableData = data.results
      this.total = data.count
    },
    changeSize(size) {
      console.log('父组件size', size)
      this.size = size
      this.fetchData()
    },
    changePage(page) {
      this.page = page
      this.fetchData()
    }
  }
}
</script>
```
### 搜索栏
编辑`src/views/blog/article/index.vue`
>template

```
<template>
  <div>
<!--    接受search子组件发送的queryData event并触发queryData函数-->
    <search @queryData="queryData"></search>
  </div>
</template>
```
>js

```
<script>
import api from '@/api/article'
import search from '@/views/blog/article/search'
export default {
  components: { search },
  data() {
    return {
      size: 10,
      page: 1,
      query: {}
    }
  },
  created() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      const { data } = await api.getAll(this.query, this.page, this.size)
      console.log('response', data)
      this.tableData = data.results
      this.total = data.count
      console.log('total', this.total)
    },
    queryData(data) {
      console.log('父组件 queryData', data)
      this.query = data
      this.fetchData()
    }
  }
}
</script>
```
编辑`src/views/blog/article/search.vue`
>template

```
<template>
<el-form :inline="true" :model="searchMap" class="demo-form-inline" size="small">
  <el-form-item>
    <el-input v-model="searchMap.title" placeholder="标题"></el-input>
  </el-form-item>
  <el-form-item>
    <el-input v-model="searchMap.category" placeholder="分类"></el-input>
  </el-form-item>
    <el-form-item>
    <el-input v-model="searchMap.tags" placeholder="标签"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="primary" @click="queryData">查询</el-button>
    <el-button type="info" @click="reset">重置</el-button>
    <el-button type="primary" @click="addData">新增</el-button>
  </el-form-item>
</el-form>
</template>
```
>js

```
<script>
import tag from '@/api/tag'
import category from '@/api/category'
export default {
  data() {
    return {
      searchMap: {
        title: '',
        category: '',
        tags: ''
      }
    }
  },
  methods: {
    queryData() {
      console.log('query', this.searchMap)
      // search子组件向index父组件发送查询数据
      this.$emit('queryData', this.searchMap)
    }
  }
}
</script>
```
### 搜索栏-重置
编辑`src/views/blog/article/search.vue`，el-form-item标签添加prop属性
```
<el-form ref="searchForm" :inline="true" :model="searchMap" class="demo-form-inline" size="small">
  <el-form-item prop="title">
    <el-input v-model="searchMap.title" placeholder="标题"></el-input>
  </el-form-item>
  <el-form-item prop="category">
    <el-input v-model="searchMap.category" placeholder="分类"></el-input>
  </el-form-item>
    <el-form-item prop="tags">
    <el-input v-model="searchMap.tags" placeholder="标签"></el-input>
  </el-form-item>
  <el-form-item>
    <el-button type="info" @click="reset('searchForm')">重置</el-button>
  </el-form-item>
</el-form>
```
添加reset方法
```
reset(formName) {
  this.$refs[formName].resetFields()
  // 通过this.$parent调用父组件fetchData方法
  this.$parent.fetchData()
},
```
### 搜索栏-编辑
#### 数据共享
使用vuex实现兄弟组件数据共享
>在项目main.js中引入store

```
import store from './store'
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
```
>在src/store/modules/article.js

```
// state设置selectList初始值
const state = {
  selectList: []
}
// mutations设置selectList值
const mutations = {
  changeSelection(state, data) {
    state.selectList = data
  }
}
// actions改变selectList值
const actions = {
  changeSelection({ commit, state }, data) {
    commit('changeSelection', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
```
>在src/store/index.js中引入article

```
import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import article from './modules/article'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    article
  },
  getters
})

export default store
```
>在src/store/getters.js中引入article

```
const getters = {
  article: state => state.article.selectList
}
export default getters
```
#### 编辑功能实现
##### 编辑对话框
编辑`src/views/blog/article/search.vue`
```
<el-dialog
      :title="title"
      width="60%"
      :visible.sync="Visible"
      center>
      <el-form label-width="40px">
        <el-form-item label="标题">
          <el-input size="small" clearable></el-input>
        </el-form-item>
        <el-form-item label="标签">
          <el-cascader size="small" style="display: block;" :options="options" :props="{ multiple: true, value: 'id', label: 'name', children: 'children' }" filterable></el-cascader>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="Visible = false">取 消</el-button>
        <el-button type="primary" @click="Visible = false">确 定</el-button>
      </div>
    </el-dialog>
```
##### 编辑markdown组件
markdown项目地址 [MarkdownEditor](http://https://github.com/hinesboy/mavonEditor)

安装markdown编辑器
```
npm install mavon-editor --save
```
引用markdown编辑器
```
<el-dialog
      :title="title"
      width="60%"
      :visible.sync="Visible"
      center>
      <el-form label-width="40px" ref="formData">
        <el-form-item label="标题">
          <el-input size="small" clearable></el-input>
        </el-form-item>
        <el-form-item label="标签">
          <el-cascader size="small" style="display: block;" :options="options" :props="{ multiple: true, value: 'id', label: 'name', children: 'children' }" filterable></el-cascader>
        </el-form-item>
        <el-form-item label="内容">
          <mavon-editor ref="md" v-model="formData.context" :editable="true"></mavon-editor>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="Visible = false">取 消</el-button>
        <el-button type="primary" @click="Visible = false">确 定</el-button>
      </div>
    </el-dialog>
```
```
<script>
import { mavonEditor } from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
export default {
  // eslint-disable-next-line vue/no-unused-components
  components: { mavonEditor },
  data() {
    return {
      formData: {} // 编辑对话框数据
    }
  },
  }
}
</script>
```