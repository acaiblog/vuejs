## npm包管理工具
### npm初始化项目
```
npm init
```
执行npm init命令之后会生成package.json文件
```
{
  "name": "vue",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}
```
package.json参数介绍
|名称|描述|
|:---|:---|
|name|包的名称|
|version|包版本|
|description|包的描述|
|main|项目入口文件|
|scripts|运行项目执行脚本|
|author|作者|

### npm安装package
查看全局安装目录
```
npm root -g
```
设置全局目录
```
npm config set prefix "d:\npm"
```
查看全局安装package列表
```
npm list -g
```
开发环境模块安装在package.json文件中的`devDependecies`字段中；生成环境模块安装在package.json文件中的`dependencies`字段中

开发环境模块安装格式
```
npm install <module_name> [--save-dev|-D]
```
比如安装eslint之后会在package.json文件中`devDependencies`字段中会自动添加`eslint`
```
npm install eslint -D
```
```
  "devDependencies": {
    "eslint": "^8.8.0"
  }
```
生产环境安装模块格式
```
npm install <module_name>[--save| -S]
```
比如安装eslint之后会在package.json文件中dependencies字段中自动添加`eslint`
```
npm install vuejs -S
```
```
  "dependencies": {
    "vuejs": "^3.0.1"
  }
```
查看远程模块最新版本
```
npm view <module_name> version
```
查看远程模块所有版本
```
npm view <module_name> versions
```
卸载局部模块
```
npm uninstall <module_name>
```
卸载全局模块
```
npm uninstall <module_name> -g
```
## Vue.js
### Vue入门开发
安装vue
```
npm install vue@2.6.10
```
>编写html页面步骤

1.采用`<script>`标签引入`vue.js`库

2.定义一个`<div>`

3.new Vue()实例化Vue应用程序
  el选项：元素element缩写，指定被Vue管理的Dom节点入口(值为选择器)，必须是一个普通的html标签节点，一般是div
  data选项：指定初始化数据，在Vue所管理的Dom节点下，可通过模板语法来进行使用

4.标签体显示数据`{{ xxxxx }}`

5.表单元素双向数据绑定`v-model`

6.注意，el的值不能为html或body

>编写html页面源码 

```
<body>
  <div id="app">
    hello, {{ msg }} <br>
<!--    v-model进行数据的双向绑定-->
    <input type="text" v-model="msg">
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  new Vue({
      el:'#app', //指定被vue管理的入口，值为选择器
      data:{ //初始化数据，在vue实例管理的dom节点下，可通过模板请求来引用
          msg:'Vue,hello'
      }
  })
</script>
```
### MVVC模型
>什么是MVVC模型？

MVVM是model-view-viewmodel的缩写，是一种软件架构风格

model：模型，数据对象；data选项中的数据

view：视图，模板页面；用于渲染数据

viewmodel：视图模型，其实本质上就是Vue实例
### 模板数据绑定渲染
可生成动态的html页面，页面中使用嵌入vue.js语法可动态生成

1.`{{ xxxx }}`双大括号文本绑定
2.`v-xxxx`以v-开头用于标签属性绑定，称为指令
3.`v-once` 一次性插值，内容只更新一次
4.`v-html`指令输出html内容
5.`v-bind`动态获取
6.`v-on`时间绑定指令，语法：v-on:事件名称="事件处理函数名"；缩写：@事件名称="事件处理函数名"；用于监听DOM事件
7.`v-model`在表单<input>,<textarea>,<select>元素上创建双向数据绑定，它负责监听用户输入事件已经更新数据

>v-html源码

```
<body>
  <div id="app">
    <p>v-html: <span v-html="contenthtml"></span></p>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          contenthtml:'<span style="color:red">内容为红色</span>'
      }
  })
</script>
```
>v-bind源码
```
<body>
  <div id="app">
    <img v-bind:src="imgUrl">
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          imgUrl:'https://cn.vuejs.org/images/logo.png'
      }
  })
</script>
```
>v-on源码

```
<body>
  <div id="app">
    <input type="text" value="1" v-model="num"> //v-model将value数据双向绑定到vue实例的num属性
    <button v-on:click="add">点击+1</button> //v-on:click="add"当点击button按钮时将add函数调用
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          num: 1
      },
      methods:{ //指定事件处理函数
          add:function (){
              console.log('add被调用')
              this.num += 1
          }
      }
  })
</script>
```
### 计算属性和监听器
#### 计算属性computed
computed选项定义计算属性，计算属性类似于methods选项中定义的函数。
>computed计算属性与methods选项定义的函数的区别?

| methods函数  | computed计算属性 |
|------------|--------------|
| 没有缓存       | 有缓存          |
| 每次都会计算     | 只有表单发生改变才会计算 |
| 函数调用需要用小括号 | 函数调用使用函数名    |


>computed源码

```
<body>
  <div id="app">
    数学分数<input type="text" v-model="math"><br>
    英语分数<input type="text" v-model="english"><br>
    函数总分<input type="text" v-model="add1()"><br>
    计算属性总分<input type="text" v-model="add2">
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          math: 80,
          english: 90
      },
      methods:{ //指定事件处理函数
          add1:function (){
              console.log('add被调用')
              return (this.math-0) + (this.english-0) //this.math-0的作用，js会将math值以字符串的方式运行-0的目的是将字符串转为数字
          }
      },
      computed:{
          add2:function (){
              console.log('add2被调用')
              return (this.math-0) + (this.english-0)
          }
      }
  })
</script>
```
#### 监听器Watch
当属性数据发生变化时，对应属性的回调函数会自动调用，在函数内部进行计算；通过watch选项或者vm实例的$watch()来监听指定的属性
- 需求：通过监听器监听，当数据发生改变时重新计算
>监听器方式1：通过watch选项监听数据分数

```
<body>
  <div id="app">
    数学分数<input type="text" v-model="math"><br>
    英语分数<input type="text" v-model="english"><br>
    函数总分<input type="text" v-model="add1"><br>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          math: 80,
          english: 90,
          add1: 0
      },
      watch:{
          math:function (newValue){ //这里的math是监听math数学分数变化的作用
              console.log('watch has call')
              this.add1 = (newValue -0) + (this.english -0)
          }
      }
  })

</script>
```
>监听器方式2：通过vm对象调用

```
<body>
  <div id="app">
    数学分数<input type="text" v-model="math"><br>
    英语分数<input type="text" v-model="english"><br>
    函数总分<input type="text" v-model="add1"><br>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          math: 80,
          english: 90,
          add1: 0
      }
  })
  vm.$watch('english',function (newValue){
      this.add1 = (newValue-0)+(this.math-0)
  })
</script>
```
### Class与Style绑定v-bind
通过class和style指定样式是数据绑定的一个常见需求，它们都是元素的属性，都用v-bind处理；其中表达式的类型可以是字符串，对象或数组
#### 语法格式
>class

语法：v-bind:class='表达式'或:class='表达式'

class的表达式可以为：
- 字符串：`:class="activeClass"`
- 对象：`:class="{active: isActive,error:hasError}"`
- 数组：`:class="['active','error']"`

>style

语法：v-bind:style='表达式'或:style='表达式'

style的表达式一般为对象：
- 对象：:style="{color:activeColor,fontSize:fontSize+'px'}"
#### 源码案例
>class

```
<head>
  <meta charset="UTF-8">
  <title>Title</title>
  <style>
    .active{
        color: green;
    }
    .error{
        color: red;
    }
    .delete{
        background-color: orange;
    }
  </style>
</head>
<body>
  <div id="app">
      <p v-bind:class="isActive">字符串表达式</p>  //字符串表达式字符串为data属性
      <p :class="{error:isError}">对象表达式</p>  //对象表达式key为样式value为data属性
      <p :class="['error','delete']">数组表达式</p>  //数组表达式元素为style元素
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
        isActive: 'active',
        isError: true
      }
  })
</script>
```
>style

```
<body>
  <div id="app">
    <p :style="{color:fontColor,fontSize:fontSize+'px'}">v-bind动态绑定样式style</p>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
        fontColor: 'red',
        fontSize: 100,
      }
  })
</script>
```
### 条件渲染v-if
#### 条件指令
- v-if 是否渲染元素
- v-else
- v-else-if
- v-show 与v-if类似，只是元素始终会被渲染并保留在dom中，只是简单切换元素的css属性display，来隐藏或显示
#### 源码
```
<body>
  <div id="app">
      <input type="checkbox" v-model="see">点击隐藏红色方块
    <div v-if="see" :style="styles"></div>
    <div v-else>红色方块隐藏了</div>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
        see:true,
        styles:{
          backgroundColor:'red',
          height: 100+'px',
          width:100+'px',
        }
      }
  })
</script>
```
#### v-if与v-show区别
>什么时候元素被渲染

- v-if如果初始条件为假什么也不做，条件为真会重新渲染元素
- v-show不管初始条件是什么，元素总是会被渲染，并且只是简单地基于css进行切换

>使用场景

- v-if有更高的切换开销
- v-show有更高的初始渲染开销
- 因此如果需要非常频繁切换使用v-show，如果运行后条件很少改变使用v-if

### 列表渲染v-for
>迭代数组

```
<body>
  <div id="app">
    <li v-for="e in emps">
      姓名：{{e.name}} 年龄：{{e.age}} 地址：{{e.addr}}
    </li>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
          emps:[
              {name:'allen',age:28,addr:'BeiJing'},
              {name:'tony',age:34,addr:'XiAn'}
          ]
      }
  })
</script>
```

>迭代对象

```
<body>
  <div id="app">
    <li v-for="(value,key,index) in objs">
      编号：{{index}} {{key}} = {{value}}
    </li>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
        objs:{
            name:'allen',
            age:28
        }
      }
  })
</script>
```
### 事件处理方法v-on或@
>格式

- 完整格式：v-on:事件名="函数名"或v-on:事件名="函数名(参数1……)"
- 缩写格式：@事件名="函数名"或@事件名="函数名(参数1……)"
- event：函数中的默认形参，代表原生dom事件；
当调用的函数有多个参数时需要使用dom事件，则通过$event作为实参传入；作用用于监听dom事件

>案例源码

```
<body>
  <div id="app">
    <button v-on:click="aaa">点击事件</button>
<!--    $event代表的时原生dom事件-->
    <button @click="bbb('hello',$event)">传入原生dom点击事件</button>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm = new Vue({
      el:'#app',
      data:{
         msg: 'hello,allen'
      },
      methods:{
          aaa:function (){
              alert(this.msg)
          },
          bbb:function (name,event) {
              //如果说函数有多个参数，而需要使用原生事件，则需要使用$event作为参数传入
              alert(name+','+event.target.tagName)
          }
      }
  })
</script>
```
#### 事件修饰符
- .stop 阻止点击事件继续传播 event.stopPropagation()
- .prevent 阻止事件默认行为 event.preventDefault()
- .once 点击事件将只会触发一次

>源码实现

```
<body>
  <div id="app">
    <div>
      <div @click="todo">
        <button @click="dothis">点击</button>
<!--        当点击button按钮时会触发todo，dothis两个函数；如果使用.stop方法则只触发dothis函数-->
        <button @click="dothis.stop">点击阻止之间继续传播</button>
      </div>
      <a href="http://www.acaicloud.com">阿才的博客</a>
<!--      默认情况下点击超链接会跳转到url，使用.prevent则阻止默认跳转yrl行为-->
      <a href="http://www.acaicloud.com" @click.prevent="herfAlert">阿才的博客</a>
    </div>
      <button @click="doOnly">点击事件触发{{num}}</button>
<!--    默认情况下点击按钮会一直加1，使用.once之后加1只会触发一次-->
      <button @click.once="doOnly">{{num}}</button>
  </div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
        num:0
      },
      methods:{
          dothis:function (){
              alert('dothis')
          },
          todo:function (){
              alert('todo')
          },
          herfAlert:function (){
              alert('welcome to acaicloud.com')
          },
          doOnly:function (){
              this.num++
          }
      }
  })
</script>
```

### 按键修饰符
>格式

v-on:keyup.按键名或@keyup.按键名

>常用按键名

- .enter
- .tab
- .delete 捕获删除和退格键
- .esc
- .space
- .up
- .down
- .left
- .right

>源码实现

```
<body>
  <div id="app">
    <input type="text" @keyup.enter="test">
  </div>
</body>
<script src="node_modules/vue/dist/vue.min.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
          num:0
      },methods:{
          test:function (){
              alert('输入回车键')
          }
      }
  })
</script>
```
### 表单数据双向绑定v-model
- 单向绑定：数据变视图变；视图变数据不变
- 双向绑定：数据变视图变；视图变数据变
>基础用法

v-model用于表单数据双向绑定，针对以下类型：
- text文本
- textarea多行文本
- radio单项按钮
- checkbox复选框
- select下拉框

>案例源码

```

```
## vue过渡动画和自定义指令
### 过渡效果
>什么时过渡效果？

元素在显示和隐藏时，实现过滤或动画的效果常用的过渡和动画都是使用CSS来实现的

在CSS中操作trasition过滤或anmation动画达到不同的效果，为目标元素添加一个父元素
<trasition name='xxx'>，让父元素通过自动应用class类名来达到效果；
过渡与动画时，会为对应元素动态添加的相关class类名：
- xxx-enter 定义显示前的效果
- xxx-enter-active 定义显示过程的效果
- xxx-enter-to 定义显示后的效果
- xxx-leave 定义隐藏前的效果
- xxx-leave-active 定义隐藏过程的效果
- xxx-leave-to 定义隐藏后的效果

>案例源码

实现渐变过渡和渐变平滑过渡
```
<head>
  <meta charset="UTF-8">
  <title>Title</title>
  <style>
    .mxg-enter-active, .mgx-leave-active{
        transition: opacity 5s; /*enter表示显示，leave表示隐藏;这里表示显示过渡效果5秒*/
    }
    .mxg-enter, .mxg-leave-to{
        opacity: 0; /*隐藏没有过渡效果*/
    }
    .meng-enter-active{ /* 显示效果 */
        transition: all 1s; /*all表示所有效果持续1s*/
    }
    .meng-leave-active{ /*隐藏效果*/
        transition: all 5s; /*all 所有效果持续5s*/
    }
    .meng-enter, .mengxuegu-leave-to{ /*显示前或隐藏后的效果*/
        opacity: 0; /*没有效果*/
        transform: translateX(20px); /*平移20px*/
    }
  </style>
</head>
<body>
  <div id="app">
    <button @click="show=!show">渐变效果</button>
<!--    当点击button按钮时show的值设置为false，所以<p>标签中show为false，<p>标签隐藏-->
    <transition name="mxg">
<!--      <transition>标签用来给<p>标签添加过渡效果，通过name属性将css过渡效果样式关联起来-->
      <p v-if="show">渐变过渡效果</p>
    </transition>
    <transition name="meng">
      <p v-if="show">渐变平滑效果</p>
    </transition>

  </div>
</body>
<script src="node_modules/vue/dist/vue.min.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
          show:true
      }
  })
</script>
```
### 动画效果
>源码案例

```
<head>
  <meta charset="UTF-8">
  <title>Title</title>
  <style>
    .bounce-enter.active{ /*显示过程中的动画效果*/
        animation: bounce-in 3s; /*bounce-in引用下面@keyframes中定义的持续3秒*/
    }
    .bounce-leave-active{ /* 隐藏过程中的动画效果 */
        animation: bounce-in 3s reverse; /*reverse相反顺序*/
    }
    @keyframes bounce-in {
        0%{ /*持续时长百分比，0%表示0秒 50%表示0.5秒 100%表示1秒*/
            transform: scale(0); /*缩小为0*/
        }
        50%{
            transform: scale(1.5);/*放大1.5倍*/
        }
        100%{
            transform: scale(1);/*原始大小*/
        }
    }
  </style>
</head>
<body>
  <div id="app">
    <button @click="show=!show">显示方法隐藏缩小动画效果</button>
<!--    第一次点击buttonshow为false，第二次点击button show为true-->
    <transition name="bounce">
<!--      transition标签的作用将p标签引入动画效果-->
        <p v-if="show">阿才的博客</p>
<!--    如果show为true显示<p>标签-->
    </transition>

  </div>
</body>
<script src="node_modules/vue/dist/vue.min.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{
          show:true
      }
  })
</script>
```
## VUE内置指令
参考：[https://cn.vuejs.org/v2/api/#](http://cn.vuejs.org/v2/api/#)
| 指令        | 描述                                 |
|-----------|------------------------------------|
| v-html    | 内容按普通html插入                        |
| v-show    | 根据表达式的真假切换元素的display CSS属性来显示或隐藏元素 |
| v-if      | 根据表达式的真假渲染元素                       |
| v-else    | 前面必须有v-if或v-else-if                |
| v-else-if | 前面必须有v-if                          |
| v-for     | 便利数组或对象                            |
| v-on      | 绑定事件监听器                            |
| v-bind    | 绑定元素属性                             |
| v-model   | 在表单或组件上创建双向绑定                      |
| v-once    | 一次性插值，当后面数据更新后视图数据不会更新 |
| v-pre     | 可以用来显示原始插入值标签{{}},并跳过这个元素和它的子元素的编译过程|

## 自定义指令
除了内置指令之外，vue也允许注册自定义指令，有的情况下你仍然需要对普通dom元进行底层操作，这时候使用自定义指令更加方便
自定义指令参考文档：[http://cn.vuejs.org/v2/guide/custom-directive.html](http://cn.vuejs.org/v2/guide/custom-directive.html)
### 注册与使用自定义指令的方式
>注册全局指令

```
//指令名不要带v-
Vue.directive('指令名',{
    // el代表使用了此指令的那个dom元素
    // binding可获取使用了此指令的绑定值
    inserted: function(el,binding){
        //逻辑代码
    }
})
```
>注册局部指令

```
directives:{
    '指令名':{
        inserted(el,binding){
            //逻辑代码
        }
    }
}
```
>使用指令

引用指令时，指令名前面加上v-；直接在元素上使用：v-指令名='表达式'

>案例演示自动转大写自定义指令

- 需求：
1. 自动转换为大写
2. 自动获取焦点

```
<body>
  <div id="app">
    <p v-upper-text="msg"></p><br>
    自动获取焦点<input type="text" v-focus>
  </div>
</body>
<script src="node_modules/vue/dist/vue.min.js"></script>
<script>
<!--    注册全局指令upper-text-->
Vue.directive('upper-text',{
    bind: function (el){ /* 参数el表示作用在元素的dom对象比如<p>标签 */
        el.style.color = 'red' /*设置p标签字体颜色为红色*/
    },
    inserted: function (el,binding){ /*el代表p标签binding可以获取使用指令的绑定值*/
        el.innerText = binding.value.toUpperCase() /*将p标签内容转换为大写*/
    }
})
  new Vue({
      el:'#app',
      data:{
          msg: 'haiyoushui'
      },
      //注册局部指令v-focus
      directives:{
          'focus':{
              inserted:function (el){
                  //聚焦元素
                  el.focus()
              }
          }
      }
  })
</script>
```
## TodoMVC项目实战
### 下载安装TodoMVC
>下载

```
git clone https://github.com/tastejs/todomvc-app-template.git
```
>安装

```
cd todomvc-app-template-master
npm install
npm install vue@2.6.10
```
### 项目初始化
编辑index.html，引入vue
```
<script src="node_modules/vue/dist/vue.js"></script>
<script src="node_modules/todomvc-common/base.js"></script>
<script src="js/app.js"></script>
```
编辑app.js
```
(function (Vue) { //表示依赖全局vue
	new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC'
		}
	})
})(Vue);

```
编辑index.html测试引入vue
```
<section class="todoapp" id="todoapp">
    <header class="header">
        <h1>{{title}}</h1>
    </header>
</section>
```
### 数据列表渲染实战
#### 功能分析
1. 有数据
- 列表中的记录有3中状态且<li>样式不一样：未完成、已完成、编辑中
- 任务字段：id、content、completed
>源码实现

编辑app.js
```
(function (Vue) { //表示依赖全局vue
	const items=[ //const在ES6中定义变量
		{id:1,content:'Vue.js',completed:false},
		{id:2,content:'Java',completed:false},
		{id:3,content:'Python',completed:false},
	]
	new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		}
	})
})(Vue);
```
编辑index.html
```
<ul class="todo-list" > <!-- 通过for循环迭代<li>标签 -->
	<li  v-for="item in items" v-bind:class="{completed:item.completed}"> <!-- 动态绑定class属性，当completed为true设置class属性为completed -->
		<div class="view">
			<input class="toggle" type="checkbox" v-model="item.completed"> <!-- 在checkbox中v-model绑定的变量为true相当于选中checkbox等同于checked -->
			<label>{{item.content}}</label> <!-- 通过items变量中content内容渲染列表 -->
			<button class="destroy"></button>
		</div>
		<input class="edit" value="Create a TodoMVC template">
	</li>
</ul>
```
2. 无数据
- .main和.footer标识的标签都应该被隐藏

```
<section class="main" v-show="items.length"> <!-- 当items列表为空时隐藏section标签 -->
    ......
</section>
<footer class="footer" v-show="items.length">
    ......
</footer>
```
### 添加任务实战
>代码实现分析

- 给input绑定一个enter事件
- 获取input输入的内容，并判断是否为空；如果为空返回return否则添加到数组
- input输入enter之后清空输入数据

>源码实现

html
```
<input @keyup.enter="addItem" class="new-todo" placeholder="What needs to be done?" autofocus >
```
js
```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		},
		methods:{
			addItem:function (event){
				const input_value = event.target.value
				//const关键字定义只读变量
				//event.target.value 获取input输入的内容
				console.log('test',input_value)
				if (input_value.length = 0 ){ //输入length等于0则代表空数据
					return  //return则返回空
				}else {
					this.items.push(
						// this.items.push 向items数组添加元素
						{
							id: items.length + 1,
							content: input_value,
							completed: false
						}
					)
					event.target.value = '' //设置input为空
				}
			}
		}
	})
```
### 未完成任务功能实现
>代码实现分析

- 当未完成任务为1时显示item否则显示items

>源码实现

html
```
				<span class="todo-count"><strong>{{ remaining }}</strong> item{{ remaining === 1 ? '' : 's' }} left</span>
<!--				remaining === 1 ? '' : 's' 三元表达式如果remaining为1为空否则添加s-->
```
js
```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		},
		computed:{
			remaining(){
				const unItems = this.items.filter(function (item){
					//filter函数过滤没有完成的数据
					//unItems返回所有未完成数据
					return !item.completed
				})
				console.log(unItems.length)
				return unItems.length //返回未完成的个数
			}
		},
```
### 全选任务实践

>需求分析

- 点击v选择所有任务：为复选框绑定计算属性，通过计算属性set方法监听如果任务更新同样更新复选框
- 当选择或取消某个任务后，复选框也同样更新

>源码实践

html
```
<input v-model="toggleAll" id="toggle-all" class="toggle-all" type="checkbox">
```
js
```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		},
		computed:{
			toggleAll:{
				get(){
					//当列表发生变化时触发该方法
					console.log('get',this.remaining)
					const test = this.remaining === 0 //当this.remaining 列表全部选择时test返回true，此时需要将复选框设置全选
					console.log(test)
					return this.remaining
				},
				set(newStatus){
					//当点击复制框之后，复选框的值会发生改变就会触发set方法调用
					//将迭代出的数组中的所有任务项，然后将当前复制框的状态值赋值给每一个任务项
					console.log(newStatus)
					//newStatus第一次点击为true，第二次点击为false，所以将newStatus赋值给items.completed实现全选
					this.items.forEach((item)=>{
						item.completed = newStatus
						//通过forEach函数将数组遍历出来，然后重新赋值实现全选
					})

				}
			}
		}
	})
```
### 移出任务项
>需求

- 点击列表❌删除列表项

>源码实现

html
```
<li  v-for="(item,index) in items" v-bind:class="{completed:item.completed}"> <!-- 动态绑定class属性，当completed为true设置class属性为completed -->
<!--						for循环取出任务项id-->
						<div class="view">
							<input class="toggle" type="checkbox" v-model="item.completed"> <!-- 在checkbox中v-model绑定的变量为true相当于选中checkbox等同于checked -->
							<label>{{item.content}}</label> <!-- 通过items变量中content内容渲染列表 -->
							<button class="destroy" @click="removeItem(index)"></button>
<!--							通过点击绑定事件函数-->
						</div>
						<input class="edit" value="Create a TodoMVC template">
					</li>
```
js
```
var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		},
		methods:{
			removeItem(index){
				console.log(index)
				this.items.splice(index,1)
			}
		}
	})
```
### 清除已完成任务项
>功能分析

- 如果任务项没有已完成隐藏清除已完成任务项
- 点击clear computed移除已完成任务项

>源码实现

html
```
<button @click="clearCompleted" class="clear-completed">Clear completed</button>
```
js
```
var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items //ES6中对象属性简写，等同于items:items;引用上面items变量
		},
		methods:{
			// clearCompleted(){
			// 	const test = this.items.filter(function (item){ //过滤已完成任务返回未完成任务
			// 		return !item.completed
			// 	})
			// 	console.log(test)
			// 	return this.items = test //将未完成任务返回给items数组
			// },
			//ES6箭头函数写法
			clearCompleted(){
				this.items = this.items.filter(item=>!item.completed)
			},
		}
	})
```
### 编辑任务功能实现
>需求分析

- 双击任务列表进入编辑状态

html
```
<ul class="todo-list" > <!-- 通过for循环迭代<li>标签 -->
					<li @dblclick="editing(item)" v-for="(item,index) in items" v-bind:class="{completed:item.completed,editing: item===editStatus}">
<!-- item===editStatus只有点击的那个任务框设置编辑状态 -->
						<!-- 动态绑定class属性，当completed为true设置class属性为completed -->
<!--						for循环取出任务项id-->
						<div class="view">
							<input  class="toggle" type="checkbox" v-model="item.completed">
							<!-- 在checkbox中v-model绑定的变量为true相当于选中checkbox等同于checked -->
							<label>{{item.content}}</label> <!-- 通过items变量中content内容渲染列表 -->
							<button class="destroy" @click="removeItem(index)"></button>
<!--							通过点击绑定事件函数-->
						</div>
						<input class="edit" :value="item.content"> //设置value为原来的数值
					</li>
				</ul>
```
js
```
var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null
		},
		methods:{
			//任务列表双击进入编辑状态
			editing(item){
				console.log('double click',item)
				this.editStatus = item //当触发双击事件时，设置任务列表为编辑状态
			},
		}
	})
```
- 取消编辑：当按下esc键时还原任务列表框
html
```
<input @keyup.esc="cancelEdit" class="edit" :value="item.content">
```
js
```
var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null
		},
		methods:{
			//取消编辑状态
			cancelEdit(){
				console.log('cancel edit')
				this.editStatus = null
			},
		}
	})
```
- 任务列表在编辑状态下按下回车保存数据
html
```
<input @keyup.enter="saveEdit(item,index,$event)" @keyup.esc="cancelEdit" class="edit" :value="item.content">
```
```
var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null
		},
		methods:{
			//按下enter保存数据
			saveEdit(item,index,event){
				console.log('save edit')
				const content = event.target.value.trim()
				//将修改的数据添加到content中
				console.log(item.content,content)
				item.content = content
				//保存数据后移除editing样式
				this.editStatus = null

			},
	})
```
### 通过自定义指令双击任务框获取焦点
>html

```
<input @keyup.enter="addItem" class="new-todo" placeholder="What needs to be done?" v-app-focus >
<input v-app-focus @keyup.enter="saveEdit(item,index,$event)" @keyup.esc="cancelEdit" class="edit" :value="item.content">
```
>js

```
	Vue.directive('app-focus',{
		inserted(el,binding){
			el.focus()
		},
		update (el,binding){
			console.log(binding.value)
			el.focus()
		}
	})
```
### 路由状态切换过滤不同数据
>需求分析

- 根据点击不同状态all、active、completed过滤不同数据
- 在data中定义变量filterStatus用于接收变化的状态值
- 通过window.onhashchage获取点击的路由hash(#开头),来获取对应的状态值赋值给filterStatus
- 定义一个计算数学filterItems用于过滤目标数据用于filterStatus的状态值变化，当变化后通过switch-case+filter过滤目标数据
- 在html页面中，将v-for中之前的items数组替换为filterItems迭代出目标数据
- 将被点击状态的</a>样式切换为.select，通过判断状态值实现

>1.根据不同点击路由将hash值赋值给data中定义的变量filterStatus

js
```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null,
			filterStatus: 'all'
		},
        }）
	window.onhashchange = function (){
		console.log('hash changed',window.location.hash)
		//window.location.hash的值；all对应#/，active对应#/active，#/completed
		//如果状态改变将hash赋值给filterStatus
		//当计算属性filterItems监听到filterStatus变化后会重新过滤
		//当filterItems重新过滤出目标数据和会自动同步到视图
		const hash = window.location.hash.substr(2) || 'all'
		//substr(2)方法截取两个字符串
		//变量hash将#/active处理成active
		console.log('hash',hash)
		app.filterStatus = hash
		//app.filterStatus赋值相当于给定义的app实例的filterStatus变量赋值
	}
```
>2.定义计算属性filterItems用于过滤目标数据，监听filterStatus的状态值变化，当变化后通过switch-case+filter过滤目标数据

js
```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null,
			filterStatus: 'all'
		},
		computed:{
			//过滤出不同状态的数据
			filterItems(){
				//this.filterStatus作为条件过滤不同数据
				switch (this.filterStatus){
					case "active":
						//过滤未完成数据
						return this.items.filter(item => !item.completed)
						break
					case "completed":
						return this.items.filter(item => item.completed)
						break
					default:
						return this.items
				}
			}
        }）
```
>3.在html中将v-for中之前的items数组替换为filterItems迭代出目标数据

html
```
<li  @dblclick="editing(item)" v-for="(item,index) in filterItems" v-bind:class="{completed:item.completed,editing: item===editStatus}">
```
>4.将被点击状态</a>样式切换为.select

html
```
<ul class="filters">
					<li>
						<a :class="{selected:filterStatus === 'all'}" href="#/">All</a>
					</li>
					<li>
						<a :class="{selected:filterStatus === 'active'}" href="#/active">Active</a>
					</li>
					<li>
						<a :class="{selected:filterStatus === 'completed'}" href="#/completed">Completed</a>
					</li>
				</ul>
```
## 数据持久化
- 将所有任务数据持久化到localStorage中，它主要适用于本地存储数据。
- 使用vue中的watch监听器监听任务数据，一旦任务数据改变则使用window.localStorage重新保存到localStorage
>1.定义监听器监听任务项数据

```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items, //ES6中对象属性简写，等同于items:items;引用上面items变量
			editStatus: null,
			filterStatus: 'all'
		},
		watch:{
			items:{ //这里items是代表监听items数据，如果items数据发生变化触发下面的handler函数
				deep:true, //deep参数代表深度监听，如果不指定则智能监听添加数据，指定deep参数则可以监听修改数据
				handler:function (newData,oldData){
					console.log('watch',newData)
				}
			}
		},
        })
```
>2.定义保存获取localStorage数据

```
(function (Vue) { //表示依赖全局vue
	const itemKey = 'item_storage'
	const itemStorage = {
		fetch:function (){ //fetch函数用来获取localstorage数据
			return JSON.parse(localStorage.getItem(itemKey) || '[]') //获取localstorage数据如果没有获取到则返回空数组，并且将数组转换为json
		},
		save:function (item){ //save函数用来保存localstorage数据
			localStorage.setItem(itemKey,JSON.stringify(item))
			//localStorage.setItem(key,value)函数两个参数key，value；key代表保存localstorage的key，value则代表数据
		}
	}
}
```
>3.vue使用localstorage获取数据，并且通过监听器保存数据

```
	var app = new Vue({
		el:'#todoapp',
		data:{
			title: 'TodoMVC',
			items: itemStorage.fetch(), //items数组数据从localstorage获取
			editStatus: null,
			filterStatus: 'all'
		},
		watch:{
			items:{ //这里items是代表监听items数据，如果items数据发生变化触发下面的handler函数
				deep:true, //deep参数代表深度监听，如果不指定则智能监听添加数据，指定deep参数则可以监听修改数据
				handler:function (newData,oldData){
					itemStorage.save(newData) //将修改的数据保存到localstorage
				}
			}
		},
})
```
## VUE过滤器和插件
### 过滤器
>什么是过滤器？

- 过滤器对要显示的文本先进行特定格式化处理，然后进行显示
>使用方式

- 全局过滤器
```
Vue.filter(过滤器名称,function(value){
    //数据处理逻辑
})
```
- 局部过滤器
```
new Vue({
    filters:{
        过滤器名称:function(value){
            //数据处理逻辑
        }
    }
})
```
- 过滤器可以用在双括号{{}}和v-bind表达式
```
<div>{{ 数据属性名称 | 过滤器名称 }}
<div>{{ 数据属性名称 | 过滤器名称(参数) }}

<div v-bind:id="数据属性名称 | 过滤器名称">
<div v-bind:id="数据属性名称 | 过滤器名称(参数)">
```
>源码案例

- 过滤敏感字符,如文本中tmd，sb过滤掉
- 过滤器传入多个参数进行数值运算

```
<body>
<div id="app">
  <p>单参数过滤器: {{ contentText | filterText }}</p>
<!--  在Vue过滤器中过滤器前面的contentText代表过滤器的第一个参数-->
  <p>多参数过滤器: {{ num1 | sumFilter(20) }}</p>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  //全局过滤器
  Vue.filter('filterText',function (value){
      if (!value){ //如果value为空
          return '数据为空未处理'
      }else {
          return value.toString().toUpperCase().replace('SB','**')
          //toString()方法将value转换为字符串
          //toUpperCase()方法将value转换为大写
          //replace(oldString,replace_string)替换字符串
      }
  })
  Vue.filter('sumFilter',function (num1,num2){
      return num1 + num2
  })
  new Vue({
      el:'#app',
      data:{
          contentText: '什么是sb',
          num1: 10
      }
  })
</script>
```
### 自定义插件
>插件的作用

- 插件通常为Vue添加全局功能，一般是全局方法/全局指令/过滤器等
- Vue插件有一个公开方法install，通过install方法给Vue添加全局功能
- 通过全局方法Vue.use()使用插件，它需要在你调用new Vue()启动应用之前完成

>定义自定义插件
新建plugin.js文件定义自定义插件
```
(function (){
    //声明MyPlugin插件对象
    const MyPlugin = {}
    MyPlugin.install = function (Vue,options){
        //1.添加全局方法
        Vue.myGlobalMethod = function (){
            alert('MyPlugin插件，全局方法myGlobalMethod被调用')
        }
        //2.添加全局指令
        Vue.directive('MyDirective',{
            inserted:function (el,binding){
                el.innerHTML = 'MyPlugin插件MyDirective指令被调用'+binding.value
            }
        })
        //3.添加实例方法
        Vue.prototype.$myMethod = function (methodOption){
            alert('Vue实例方法生效'+methodOption)
        }
    }
    //将插件添加到window对象中
    window.MyPlugin = MyPlugin
})() //括号的作用是自动运行
```
>调用自定义插件

```
<body>
<div id="app">
  <p>引用全局指令MyDirective</p>
  <span v-my-directive="content"></span>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script src="js/plugins.js"></script>
<script>
  //引入自定义插件
  Vue.use(MyPlugin)
  var vm = new Vue({
      el:'#app',
      data:{
          content: 'hello'
      }
  })
  //调用自定义的全局方法
  Vue.myGlobalMethod()
  //调用Vue实例方法
  vm.$myMethod('Allen')
</script>
```
## Vue组件化开发
>什么是组件？

Vue中的组件化开发就是把网页的重复代码抽取出来封装成一个个可复用的视图组件，然后将这些组件凭借到一起构成一个完成的系统。这种方式非常灵活可以提供开发和维护效率。

组件就是对局部视图的封装，每个组件包含了：html结构、css、js(data和methods)
### 组件的使用
为了能够在模板中使用，这些组件必须先注册以便Vue能够识别，注册组件分为两种类型：全局注册和局部注册
#### 全局注册
>全局组件注册语法

```
Vue.commponent('',{
    template:'定义组件模板',
    data:function(){
        return {}
    }
})
```
说明：
- 组件命名建议使用横线分隔符
- template定义组件的视图模板
- data在组件中必须是一个函数
>实例代码

```
<body>
<div id="app">
  <component-a></component-a>
<!--  引用组件直接使用组件名称-->
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  Vue.component('component-a',{
      template:'<p>这是一个组件{{name}}</p>',
      data:function (){
          return {
              name: 'Allen'
          }
      }
  })
  new Vue({
      el:'#app'
  })
</script>
```
#### 局部注册
一般把不是通用的组件，注册为局部组件
>源码实现

```
<body>
<div id="app">
  <component-a></component-a>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
    var CompenentB = {
        template:'<div>这是局部注册组件:{{name}}</div>',
        data:function (){
            return {
                name:'Allen'
            }
        }
    }
  new Vue({
      el:'#app',
      data:{},
      components:{
          'component-a':CompenentB //key为局部注册组件名称，value为组件对象
      }
  })
</script>
```
### 多组件案例

>定义组件

header-component.js
```
Vue.component('header-component',{
    template:'<div>这是头部组件</div>'
})
```
main-component.js
```
Vue.component('main-component',{
    template:'<div><ul><li>博客</li><li>标签</li><li>分类</li></ul></div>'
})
```
foot-component.js
```
Vue.component('foot-component',{
    template:'<div>这是foot组件</div>'
})
```
>引用组件

```
<body>
<div id="app">
  <header-component></header-component>
  <main-component></main-component>
  <foot-component></foot-component>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script src="components/header.js"></script>
<script src="components/main.js"></script>
<script src="components/foot.js"></script>
<script>
  new Vue({
      el:'#app',
  })
</script>
```
### Bootstrap组件案例
代码下载地址：
```
https://github.com/ChaplainsBud/bootstrap5-example-dashboard.git
```
将dashboard可以拆分为5个部分，导航栏、左侧菜单、右侧主页面；右侧主页面又分为上半部分和下半部分
![输入图片说明](img/layout.png)
#### 多组件开发
##### 导航栏
创建components目录，将index.html页面放到同一个div标签下
![输入图片说明](img/1.pngimage.png)
>引入vue js

```
<script src="../node_modules/vue/dist/vue.js"></script>
<script src="components/header.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{},
      components:{
          AppHeader
      }
  })
</script>
```
>将导航栏单独放到heander.js文件中

创建components/heander.js文件
```
;(function (){
   window.AppHeader = {
       template:`<header
      class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow"
    >
      <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#"
        >Company name</a
      >
      <button
        class="navbar-toggler position-absolute d-md-none collapsed"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#sidebarMenu"
        aria-controls="sidebarMenu"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <input
        class="form-control form-control-dark w-100"
        type="text"
        placeholder="Search"
        aria-label="Search"
      />
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </header>`
   }
})()
```
##### 菜单栏
>定义menu组件

创建components/menu.js
```
;(function(){
    window.AppMenu = {
        template:`<nav...>`
    }
})
```
>index.html页面引入单独的menu组件

```
<script src="../node_modules/vue/dist/vue.js"></script>
<script src="components/header.js"></script>
<script src="components/menu.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{},
      components:{
          AppHeader,
          AppMenu
      }
  })
</script>
```
##### 主页面
主页面分为两个部分，dashboard和tab表格。可以使用vue子组件的方式实现。
>定义home主组件

在components/home/AppHome.js中将右侧主页面内容抽取到AppHome.js中
![输入图片说明](img/main_page.png)

在index.html中引入定义的home主组件
```
<body cz-shortcut-listen="true">
<div id="app">
<!--        主页面-->
    <app-home></app-home>
  </div>
</div>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>
<script src="components/header.js"></script>
<script src="components/menu.js"></script>
<script src="components/home/AppHome.js"></script>
<script>
  new Vue({
      el:'#app',
      data:{},
      components:{
          AppHeader,
          AppMenu,
          AppHome
      }
  })
</script>
```
>在home主组件中定义dashboard子组件

在components/home/dashboard.js中定义dashboard子组件

![输入图片说明](img/dashboard.png)

在components/home/AppHome.js中引用dashboard子组件
```
window.AppHome = {
   template,
    components:{
        DashBoard
    }
}
```
在components/home/AppHome.js中使用dashboard标签

![输入图片说明](img/dashboard1.png)

>在home主组件中定义table子组件

在components/home/table.js中定义table子组件
![输入图片说明](img/AppTable.png)
在components/home/AppHome.js中引入table子组件,并在table标签使用app-table标签引入
![输入图片说明](img/table1.png)
#### 极致化组件开发
极致化组件开发就是将组件分为一个根组件和五个子组件；在index.html页面引用App.js根组件，然后再App.js根组件中包含五个子组件;根组件就是将index.html页面body标签中html元素抽取到根组件中，需要注意的是根故组件有且仅有一个根标签
在项目根目录下定义App.js

![输入图片说明](img/apps.png)

在index.html页面中引用
```
<body>
    <div id="app">
      <app></app>
    </div>
    <script src="../node_modules/vue/dist/vue.js"></script>
    <script src="components/header.js"></script>
    <script src="components/menu.js"></script>
    <script src="components/home/dashboard.js"></script>
    <script src="components/home/table.js"></script>
<!--    子组件引入js在主组件js前面-->
    <script src="components/home/AppHome.js"></script>
    <script src="App.js"></script>
    <script>
      new Vue({
          el:'#app',
          data:{},
          //vue组件默认会多出来一个<div id=app></div>的标签，template属性的作用就是将<div id=app></div>替换为<div></div>
          template:'<app></app>',
          components:{
              app
          }
      })
</body>
```
### Vue组件化注意事项
- 组件可以理解为一个特殊的VUE实例，不需要手动实例化，管理自己的模板
- 组件的template有且只有一个根节点
- 组件data必须是一个函数，函数的返回值必须是一个对象
- 组件和组件之间相互是独立的，可以配置自己的data、methods、computed等
- 组件的思想，自己管理自己不影响别人
### Vue组件通信
#### 组件通信方式
- props父组件向子组件传递数据
- $emit 自定义事件
- slot插槽分发内容
#### 组件间通信规则
- 不要再子组件中直接修改父组件传递的数据
- 数据初始化时，应当看初始化的数据是否用于多个组件中。如果需要被用于多个组件中，则初始化在负组件中；
如果只在一个组件中使用，那就初始化在这个要使用的组件中。
- 数据初始化在哪个组件，更新数据的方法(函数)就应该定义在哪个组件。
#### props向子组件传递数据
>声明组件对象中定义props

在声明组件对象中使用props选项指定
```
const MyComponent = {
    template:'<div></div>',
    props: 此处值由以下3中方式,
    components:{
        
    }
}
```
方式1：指定传递属性名，注意是数组形式
```
props:['id','name']
```
方式2：指定传递属性名和数据类型，注意是对象形式
```
props:{
    id:number,
    name:string
}
```
方式3：指定属性名、数据类型、必要性、默认值
```
props:{
    name:{
        type:string,
        required:true,
        default:'msg'
    }
}
```
>引用组件时动态赋值

```
<my-component v-bind:id="1" :name="allen"></my-component>
```
>传递数据注意

- props只用于父组件向子组件传递数据
- 所有标签属性都会成为组件对象的属性，模板页面可以直接引用
- 如果需要向非子后代传递数据，必须多层逐层传递
- 兄弟组件间也不能直接props通信，必须借助父组件才可以
#### props案例-列表渲染
需求：table组件渲染数据功能

##### 数组形式实现
>实现步骤

父组件中定义data，给data设置infos属性
```
window.HomeMain = {
   template,
    data(){
      return{
          infos:['coding','watch books','sleeping']
      }
    },
    components:{
       HomeMenu,
        HomeTable
    }
}
```
在父组件中，子组件标签处通过v-bind绑定要传递给子组件的数据
```
<div>
    <home-table :infos="infos"></home-table>
</div>
```
在子组件中通过props指定接受父组件的属性
```
window.ParagraphComponent = {
   props:['hobbies'],
   template
}
```
在子组件中的div标签通过v-for循环需要数据
```
<div v-for="(info,index) in infos" :key="index">
    <h1>{{info}}</h1>
</div>
```
##### 对象形式实现
>实现步骤

父组件中定义data，给data设置infos属性
```
window.HomeMain={
   template,
    data(){
      return{
          infos:[
              {id:1,name:'Allen',age:28,addr:'XiAn'}
          ]
      }
    },
    components:{
       HomeMenu,
        HomeTable
    }
}
```
在父组件中，子组件标签处通过v-bind绑定要传递给子组件的数据
```
<home-table :infos="infos"></home-table>
```
在子组件中通过props指定接受父组件的属性
```
window.HomeTable={
   template,
    props:{
        infos: Array
    }
}
```
在子组件中的div标签通过v-for循环需要数据
```
<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Age</th>
      <th>Addr</th>
    </tr>
  </thead>
  <tbody v-for="(info,index) in infos" :key="index">
    <tr>
      <td>{{info.id}}</td>
      <td>{{info.name}}</td>
      <td>{{info.age}}</td>
      <td>{{info.addr}}</td>
    </tr>
  </tbody>
</table>
```
#### props案例：删除列表数据
要删除哪个数据就在定义数据的组件中定义一个删除函数，在HomeMain组件中定义删除函数
```
window.HomeMain={
   template,
    data(){
      return{
          infos:[
              {id:1,name:'Allen',age:28,addr:'XiAn'}
          ]
      }
    },
    methods:{
      delete_list(index){ //index是要删除数据的下标相当于id
          this.infos.splice(index,1)
      }
    },
    components:{
       HomeMenu,
        HomeTable
    }
}
```
然后通过数据绑定的方式将delete_list函数传递给子组件
```
<home-table :infos="infos" :delete_list="delete_list"></home-table>
```
在子组件定义一个删除数据的方法
```
window.HomeTable={
   template,
    props:{
        infos: Array,
        delete_list:Function, //指定接受删除数据函数
        index:Number  //指定接受删除数据index
    },
    methods:{
       deleteItem(){
           this.delete_list(this.index) //调用父组件传递过来的删除函数和index
       }
    }
}
```
定义一个a标签，点击a标签绑定deleteItem函数，删除列表数据
```
<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Age</th>
      <th>Addr</th>
    </tr>
  </thead>
  <tbody v-for="(info,index) in infos" :key="index">
    <tr>
      <td>{{info.id}}</td>
      <td>{{info.name}}</td>
      <td>{{info.age}}</td>
      <td>{{info.addr}}</td>
      <td><a href="#" @click="deleteItem">Delete Data</a></td>
    </tr>
  </tbody>
</table>
```
#### 自定义事件
自定义事件用来代替props传入函数形式
>绑定自定义事件

在父组件中定义事件监听函数，并引用子组件标签上v-on绑定事件监听
```
//通过v-on绑定
//@自定义事件名=事件监听函数
//在子组件table中触发delete_list事件来调用deleteItem函数
<home-table @delete_list="deleteItem"></home-table>
```
>触发监听事件函数执行

在子组件中触发父组件的监听事件函数调用
```
//子组件触发事件函数调用
//this.$emit(自定义事件名,data)
//this.$emit('delete_list',index)
```
>自定义事件注意点

- 自定义事件只用于子组件向父组件发送消息
- 隔代组件或兄弟组件间通讯不适用于此种方式
>自定义事件案例

自定义事件是通过子组件触发父组件的监听事件函数进行执行删除操作,在父组件HomeMain定义deleteInfo函数
```
window.HomeMain={
   template,
    data(){
      return{
          infos:[
              {id:1,name:'Allen',age:28,addr:'XiAn'}
          ]
      }
    },
    methods:{
      delete_list(index){ //index是要删除数据的下标相当于id
          this.infos.splice(index,1)
      },
        deleteInfo(index){
          this.infos.splice(index,1)
        }
    },
    components:{
       HomeMenu,
        HomeTable
    }
}
```
在父组件HomeMain组件<home-table>标签绑定deleteInfo函数
```
<home-table @delete_info="deleteInfo"></home-table> <!-- delete_info是自定义的事件名，deleteInfo是自定义函数名 -->
```
在子组件HomeTable组件定义删除数据函数
```
window.HomeTable={
   template,
    props:{
        infos: Array,
    },
    methods:{
        delete_func(){
           this.$emit('delete_info',index) //触发父组件HomeMain中的delete_info事件
        }
    }
}
```
在子组件触发父组件的监听事件函数执行删除操作
```
<a href="#" @click="delete_func">Delete Data</a>
```
#### slot插槽
作用：主要用于父组件向子组件传递标签和数据，而prop和自定义事件只是传递数据
应用场景：一般是某个位置需要经常动态切换标签显示效果
>子组件定义插槽

```
<div>
    <slot name="aaa">不确定的标签结构1</slot>
    <div>组件确定的标签结构</div>
    <slot name="bbb">不确定的标签结构2</slot>
</div>
```
>父组件传递标签数据

```
<child>
    <div slot="aaa">向name=aaa的插槽处插入此标签数据</div>
    <div slot="bbb">向name=bbb的插槽处插入此标签数据</div>
</child>
```
>案例：通过插槽动态显示标题

需求：将Section title设置为插槽

在HomeTable子组件设置插槽
```
<div>
  <slot name="TableSlot"></slot>
</div>
```
在父组件HomeMain中向子组件定义的插槽处传递标签数据
```
<home-table>
  <h1 slot="SelectTable">{{title}}</h1>
</home-table>
```
在父组件vue中定义要返回title数据
```
window.HomeMain={
   template,
    data(){
      return{
          title:'仪表盘',
      }
    }
}
```
#### 非父子组件数据通讯
>介绍

Vue.js可以通过PubSubJS库来实现非父子组件之间通信，使用PubSubJS的消息发布与订阅模式来进行数据的传递。

理解：订阅信息等同于绑定事件监听，发布消息等同于触发事件。必须先执行订阅事件subscribe才能publish发布事件

安装PubSubJS库
```
npm install pubsub-js
```
引入js
```
<script src="node_modules/pubsub-js/src/pubsub.js"></script>
```
>订阅消息(绑定事件监听)

现在create钩子函数中订阅消息
```
window.HomeMain={
   template,
    create(){
        PubSub.subscribe('消息名称',function (event,data){
            //事件回调处理
        })
    },
}
```
>发布消息

```
PubSub.publish('消息名称',data)
```
>案例

需求：删除右侧表格数据在左侧菜单栏显示已删除的总数，没有删除则不显示数量

引入js
```
<script src="node_modules/pubsub-js/src/pubsub.js"></script>
```
在HomeMenu子组件中create狗子中订阅消息，监听当删除爱好后进行统计已删除总数
```
window.HomeMenu={
   template,
    data(){
      return {
          delNum:0 //初始化显示数量
      } 
    },
    create(){
       PubSub.subscribe('changeNum',(event,num)=>{
           //删除成功
           this.delNum = this.delNum+num
       })
    }
}
```
在HomeMain父组件中发布消息
```
window.HomeMain={
       template,
        data(){
          return{
              title:'仪表盘',
              infos:[
                  {id:1,name:'Allen',age:28,addr:'XiAn'}
              ]
          }
        },
        methods:{
          delete_list(index){ //index是要删除数据的下标相当于id
              this.infos.splice(index,1)
              //删除成功发布消息，
              PubSub.publish('changeNum',1)
          },
            deleteInfo(index){
              this.infos.splice(index,1)
            }
        },
        components:{
           HomeMenu,
            HomeTable
        }
    }
```
在HomeMenu组件添加span标签显示删除数量
```
<a class="nav-link active" href="#">
  <span data-feather="home"></span>
  Dashboard <span class="sr-only">(current)</span>
  <span v-show="delNum">({{delNum}})</span>
</a>
```
## 加载效果
当页面请求数据时进行加载效果，请求数据完成关闭加载效果。编辑src/utils/request.js
```
// eslint-disable-next-line no-unused-vars
import axios from 'axios';
import {Loading,Message} from "element-ui";

//定义加载效果
const loading = {
    loadingInstance: null,
    open:function (){
        if(this.loadingInstance == null){
            this.loadingInstance = Loading.service({
                target:'.main'
            })
        }
    },
    close:function (){
        if(this.loadingInstance !== null){
            this.loadingInstance.close()
        }
    }
}

// eslint-disable-next-line no-unused-vars
const  axios_request = axios.create({
    baseURI: 'http://127.0.0.1:8000/api',
    timeout: 5000
})
//请求拦截器打开加载效果
axios_request.interceptors.request.use(config =>{
    loading.open() //打开加载效果
    return config
},error =>{
    loading.close() //关闭加载效果
    return Promise.reject(error)
})

//响应拦截器 和异常中关闭加载效果
axios_request.interceptors.response.use(response =>{
    loading.close()
    return response
},error =>{
    loading.close()
    return Promise.reject(error)
})
export default axios_request //导出自定义创建axios对象
```
## axios请求异常处理
在axios响应拦截器中处理异常编辑src/utils/request.js
```
//响应拦截器 和异常中关闭加载效果
axios_request.interceptors.response.use(response =>{
    loading.close()
    //当 请求接口code代码不是200说明后台服务异常可在此统一处理
    if(response.data.code !== 200){
        Message({
            message:response.data.data.msg
        })
    }
    return response
},error =>{
    loading.close()
    //当接口异常时，进行弹出错误提示
    Message({
        message:error.message,
        type:'error',
        duration: 5*1000
    })
    return Promise.reject(error)
})
```