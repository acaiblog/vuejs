# 什么是Vuex
>官方解释

Vuex是一个转为vue.js应用程序开发的状态管理模式，它采用集中式存储管理应用的所有组件的状态。并以响应的规则保证状态以以重可预测的方式发生变化。
>简单理解

Vue应用中的每个组件在data()中封装这自己的数据属性，而这些data属性都是私有的完全隔离的

如果我们希望多个组件都能读取到同一状态数据属性，或者不同组件的行为需要更新同一状态数据属性就需要一个将共享的数据属性进行集中式的管理。这就是vuex状态管理索要解决的问题。
# State 读取状态值
>概念

每个Vuex项目的核心就是store，store就是一个对象包含着项目中大部分state。state是store对象中的一个选项，是Vuex管理的状态对象(共享的数据属性)
>实践

创建一个vue项目
```
vue ui
```
在src目录下创建store目录，store下创建index.js
```
import Vue from 'vue'
import Vuex from 'vuex'

//引入vuex
Vue.use(Vuex)

const store = new Vuex.store({
    state:{
        count: 1
    }
})

export default store
```
在main.js中注册store
```
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```
home组件中读取state状态数据，编辑src/views/home.vue
```
<template>
  <div>
    count:{{ $store.state.count }}
  </div>
</template>
```
# mutation 改变状态值
>概念

在store的mutations选项中定义的方法才可以改变状态(state)值，通过$store.commit('mutationName')触发状态值的改变
>实践

修改store下index.js在store中添加mutations选项
```
import Vue from 'vue'
import Vuex from 'vuex'

//引入vuex
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        count:1
    },
//    改变state值
    mutations:{
        increment(state){
            state.count ++
        },
        decrement(state){
            state.count --
        }
    }
})
export default store
```
在Home.vue中中调用mutations中定义的方法
```
<template>
  <div>
    count:{{ $store.state.count }}
    <button @click="addCount">加</button>
    <button @click="delCount">减</button>
  </div>
</template>
<script>
export default {
  methods:{
    addCount(){
      this.$store.commit('increment')
    },
    delCount(){
      this.$store.commit('decrement')
    }
  }
}
</script>
```
## 不同组件共享状态值state
在不刷新页面的情况下切换路由实现共享state
>实践

修改src/views/About.vue，获取值count,点击加/减button并切换页面发现state状态值的count可以共享
```
<template>
  <div class="about">
    About: {{ $store.state.count }}
  </div>
</template>
```
## 提交载荷
可以向$store.commit传入额外的参数即mutation的载荷palyload,编辑src/store下的index.js
```
const store = new Vuex.Store({
    state:{
        count:1
    },
//    改变state值
    mutations:{
        increment(state,n){
            state.count += n
        }
    }
})
export default store
```
编辑home组件
```
<script>
export default {
  methods:{
    addCount(){
      this.$store.commit('increment',10)
    }
  }
}
</script>
```
# Action改变state状态值
>概念

action的功能和mutation类似都是用来改变state状态值，不同在于action提交的是mutation，而不是在组件中直接变更state状态，通过action间接更新state

在组件中通过this.$store.dispatch('actionName')触发state进行改变，action也支持载荷和任意异步操作
>实践

编辑src/store/index.js增加actions选项
```
const store = new Vuex.Store({
    state:{
        count:1
    },
//    改变state值
    mutations:{
        increment(state,n){
            state.count += n
        },
        decrement(state){
            state.count --
        }
    },
    actions:{
        add(context,n){
            context.commit('increment',n)
        },
        decrement({commit,state}){
            commit('decrement')
        }
    }
})
export default store
```
编辑views/Home.vue触发action改变值
```
<script>
export default {
  methods:{
    addCount(){
      this.$store.dispatch('add',10)
    },
    delCount(){
      this.$store.dispatch('decrement')
    }
  }
}
</script>
```
# 派生属性getter
>概念

有时候需要从store中的state中派生出一些状态属性，getter其实就是类似于计算属性get的对象，组件中通过$store.getters.xxx读取
>实践

store/index.js中增加getters选项
```
const store = new Vuex.Store({
    state:{
        count:1
    },
    getters:{
        desc(state){
            if(state.count <50){
                return 'test1'
            }else {
                return 'test2'
            }
        }
    }
})
export default store
```
views/Home.vue获取派生属性desc的值
```
<template>
  <div>
    desc: {{ $store.getters.desc }}
  </div>
</template>
```
# module模块化项目结构
由于使用单一状态数，应用的所有状态会集中到一个较大的对象，当应用变得非常复杂时，store对象就有可能变得相当臃肿。为了解决以上问题vuex允许我们将store分割成模块，每个模块拥有自己的state、mutation、action、getter等
```
const moduleA = {
state: { ... },
mutations: { ... },
actions: { ... },
getters: { ... }
}
const moduleB = {
state: { ... },
mutations: { ... },
actions: { ... }
}
const store = new Vuex.Store({
modules: {
a: moduleA,
b: moduleB
}
})
store.state.a // -> moduleA 的状态
store.state.b // -> moduleB 的状态
```