# 什么是webpack
webpack式一个前端的静态模块资源打包工具，能让浏览器支持模块化。它根据模块的依赖关系
进行静态分析，然后将这些模块按照指定的规则生成对应的静态资源
# webpack安装
>全局安装

安装webpack
```
安装最新版本
npm install --global webpack
安装指定版本
npm install --global webpack@<version>
```
如果安装的式webpack v4+还需要安装cli才能使用webpack命令行
```
npm install --global webpack-cli
```
# webpack快速入门
>全局安装webpack

```
npm install --global webpack@v4.35.2
```
安装webpack cli
```
npm install --global webpack-cli@3.3.6
```
查看webpack版本
```
webpack -v
```
>本地安装webpack

```
npm install --save-dev webpack@v4.35.2 webpack-cli@3.3.6
```
创建目录结构
```
webpack
└─js
   └─bar.js
   └─main.js
 └─index.html
```
在bar.js中编写
```
//node.js模块化编程
module.export = function (){
    console.log('bar js')
}
```
在main.js中引入bar.js中的函数
```
var bar = require('./bar.js')

bar()
```
使用node命令测试js代码没有语法错误
```
node js/main.js
```
默认浏览器不识别模块化编程语法，需要通过webpack进行编译
```
webpack js/main.js -o js/bundle.js
```
# webpack项目实践
目录结构：
```
└─demo
    └─src
       └─bar.js
       └─main.js
    └─index.html

```
webpack编译js
```
webpack ./src/main.js -o dist/jundle.js
```
index.html
```
<script src="dist/bundle.js"></script>
```
>打包webpack config.js配置文件


在项目路径创建webpack.config.js
```
const path = require('path') //引入node中path模块，处理文件路径

//导出一个webpack具有特殊属性配置的对象
module.exports = {
    mode: 'development', //development,production,none三种模式
    //入口
    entry: './src/main.js', //入口模块路径
    //出口
    output: {
        path: path.join(__dirname,'./dist/'),
        filename: 'bundle.js'
    }
}
```
执行webpack命令自动编译main.js
```
webpack
```
# webpack初始化项目
初始化demo1
```
cd webpack/demo1
npm init -y
```
本地安装webpack
```
npm install --save-dev webpack webpack-cli
```
本地安装的webpack不能使用webpack命令需要使用package.json中scripts配置
```
{
  "name": "demo1",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "show": "webpack -v"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "webpack": "^5.72.0",
    "webpack-cli": "^4.9.2"
  }
}
```
执行npm run show，显示webpack版本
```
npm run show
```
package.json命令映射build
```
"scripts": {
    "show": "webpack -v",
    "build": "webpack"
  },
```
执行build映射命令编译代码
```
npm run build
```
package.json命令映射start
```
"scripts": {
    "show": "webpack -v",
    "build": "webpack",
    "start": "node ./src/main.js"
  },
```
执行npm run start返回main.js console输出内容
```
npm run start
```
# ES6模块规范
## 导出默认成员
默认成员智能有一个否则会报错，编辑bar.js
```
export default function (){
    console.log("i'm es6")
}
```
编辑main.js
```
import bar from './bar'
bar() //运行bar函数
```
webpack编译
```
npm run build
```
## ES6导出非默认成员
编辑bar.js导出多个非默认成员
```
export const x = 'xxx'
export const y = 'yyy'
export function add(a,b){
    return a+b
}
```
编辑main.js引入多个非默认成员
```
//es6
import {x,y,add} from './bar'
console.log(x,y,add(10,20))
```
配置自动编译
```
  "scripts": {
    "show": "webpack -v",
    "build": "webpack",
    "start": "node ./src/main.js",
    "watch": "webpack --watch"
  },
```
执行命令，刷新浏览器自动编译代码
```
npm run watch
```
# 打包css/image等资源
webpack本身智能处理javascript模块，如果要处理其他类型的文件，就需要结合插件来使用，这些插件在webpack 中被称为loader加载器来转换

loader可以理解为模块和资源的转换器，它本身是一个函数，参数接受的是源文件，返回值是转换后的结果。这样我们就可以通过require来加载任何类型的模块或文件，比如css、image

## 打包css资源
安装loader模块
```
npm install --save-dev style-loader@3.0.0 css-loader@0.23.1
```
webpack.config.js配置module
```
const path = require('path') //引入node中path模块，处理文件路径

//导出一个webpack具有特殊属性配置的对象
module.exports = {
    mode: 'development', //development,production,none三种模式
    //入口
    entry: './src/main.js', //入口模块路径
    //出口
    output: {
        path: path.join(__dirname,'./dist/'),
        filename: 'bundle.js'
    },
    module:{
        rules:[
            {
                test: /\.css$/, //正则表达式，匹配.css资源
                use:[
                    'style-loader', //js识别css
                    'css-loader' //css转换未js
                ]
            }
        ]
    }
}
```
src/css/style.css
```
body{
    background-color: red;
}
```
main.js引入css
```
//将样式文件css以模块化形式引入
import './css/style.css'
```
## 打包图片资源
安装file-loader模块
```
npm install --save-dev file-loader@4.0.0
```
webpack.config.js配置匹配规则
```
module:{
        rules:[
            {
                test: /\.css$/, //正则表达式，匹配.css资源
                use:[
                    'style-loader', //js识别css
                    'css-loader' //css转换未js
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use:[
                    'file-loader'
                ]
            }
        ]
    }
```
编译
```
npm run build
```
copy index.html到dist目录，访问dist/index.html测试
## htmlwebpackplugin插件
用来解决文件路径问题，将index.html打包到dist目录下；同时会在index.html中自动引入bundle.js文件
安装插件
```
npm install --save-dev html-webpack-plugin@3.2.0
```
编辑webpack.config.js配置插件
```
const path = require('path') //引入node中path模块，处理文件路径
const HtmlWebpackPlugin = require('html-webpack-plugin')

//导出一个webpack具有特殊属性配置的对象
module.exports = {
    ...
    //配置htmlwebpackplugin插件
    plugins: [
      new HtmlWebpackPlugin({
          template: 'index.html' //指定要打包的模板文件会将模板文件打包到和bundle同一目录下;会在index.html中自动引入bundle.js
      })
    ],
    module:{
        ...
    }
}
```
## webpack自动打包
使用webpack-dev-server模块实现自动打包代码并自动刷新浏览器
安装模块
```
npm install --save-dev webpack-dev-server@3.7.2
```
编辑webpack.config.js配置
```
//实时重新加载
devServer: {
  contentBase: './dist'
},
```
编辑package.json配置
```
  "scripts": {
    "dev": "webpack-dev-server --open"
  },
```
执行命令
```
npm run dev
```
# Babel解决浏览器兼容性

安装模块
```
npm install -D babel-loader@8.0.6 @babel/core@7.5.4 @babel/preset-env@7.5.4
```
编辑webpack.config.js配置
```
module:{
        rules:[
            //解决兼容性问题
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
```
# Vue loader与webpack集成
Vue Loader是一个webpack loader，可以构建vue单文件组件

安装依赖包
```
npm install -D vue-loader@15.7.0 vue-template-compiler@2.6.10
```

>webpack配置

引入vue-loader插件
```
const VueLoaderPlugin = require('vue-loader/lib/plugin')
```
配置匹配规则
```
module:{
    rules:[
        //匹配.vue单文件组件
        {
            test: /\.vue$/,
            loader: "vue-loader"
        }
    ]
}
```
加载plugins
```
plugins: [
    new VueLoaderPlugin()
],
```
创建App.vue
```
<template>
  <div>
    <h1>App</h1>
  </div>
</template>
```
在main.js中引入app.vue
```
import App from './App.vue'
```
编译
```
npm run build
```
# import导入vue指定版本

安装vue
```
npm install -D vue@2.6.10
```

在webpack.config.j2中定义
```
module.exports = {
    module:{
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.js'
        }
    }
}
```
定义App.vue组件
```
<template>
  <div>
    <h1>App Root Components</h1>
  </div>
</template>
```
index.html引入vue单组件
```
<body>
<div id="app">
  <app></app>
</div>
</body>
```
main.js创建vue实例
```
import Vue from 'vue'
import App from './App.vue'

new Vue({
    el:'app',
    template: '<App/>',
    components:{App}
})
```
编译运行
```
npm run build
```
# Vue采用render函数渲染组件

```
import Vue from 'vue'
import App from './App.vue'

new Vue({
    el:'app',
    // template: '<App/>',
    //template实质上没有编译和渲染功能，而当前编译功能可以直接采用vue-loader进行编译
    //而渲染功能实质上是通过render函数来进行渲染组件，所以只需要在此处指定render渲染组件即可
    render: function (h){
        return h(App)
    },
    //components:{App}
})
```
使用箭头函数简写
```
import Vue from 'vue'
import App from './App.vue'

new Vue({
    el:'app',
    render: h => h(App)
})
```
另一种写法更简洁
```
new Vue({
    render: h => h(App)
}).$mount('#app')
``
# Webpack模块热替换HMR
模块热替换不需要全部刷新页面，局部无刷新的情况下更新
>配置

```
//webpack.config.js
const webpack = require('webpack')
module.exports = {
    //实时重新加载
    devServer: {
      hot: true
    },
    //配置htmlwebpackplugin插件
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],

}
```