# 生命周期
>生命周期钩子函数

每个Vue实例在被创建时都要经过一系列初始化过程，生命周期分为三大阶段：初始化显示、更新显示、销毁VUe实例

初始化阶段钩子函数：
```
beforeCreate()实例创建前触发，数据和模板均未获取到
created()实例创建后触发，最早可访问到data数据，但模板未获取到
beforeMount()数据挂载前触发，模板获取到，但是数据未挂在到模板上
mounted()数据挂在后触发，数据已挂载到模板中
```
更新阶段的钩子函数：
```
beforeUpdate()模板更新前触发，data改变后，更新数据模板前调用
update()模板更新后触发，将data渲染到数据模板中
```
销毁阶段的钩子函数：
```
beforeDestroy()实例销毁前触发
destroyed()实例销毁后触发
```
>Vue生命周期代码演示

```
<body>
<div id="app">{{msg}}</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script>
  var vm=new Vue({
      data(){
          return{
              msg:'Allen'
          }
      },
      beforeCreate(){
          //实例创建前触发，数据和模板均未获取到
          console.log('beforeCreate',this.$el,this.data)
      },
      created(){
          console.log('created',this.$el,this.data)
      },
      beforeMount(){
          //模板已获取到，但是数据未挂载到模板
          console.log('beforemount',this.$el,this.data)
      },
      mounted(){
          //数据挂载到模板
          console.log('mounted',this.$el,this.data)
      },
      beforeUpdate() {
          //当data改变后更新模板中的数据前调用
          console.log('beforeupdate',this.$el,this.data)
      },
      updated(){
          //data被Vue渲染之后的Dom数据模板
          console.log('updated',this.$el.innerHTML,this.data)
      },
      beforeDestroy(){
          //销毁实例前调用
          console.log('beforeDestroy')
      },
      destroyed(){
          //销毁实例后调用
          console.log('created')
      }
  }).$mount('#app')
</script>
```
# Ajax库
Vue常用Ajax库有两种Vue-resource和axios；在vue1.x版本中推荐vue-resource，vue2.x版本中推荐axios。
VS Code需要安装liveserver模拟Ajax请求
![输入图片说明](img/liveServer_install_plugin.png)
>vue-resource

安装vue-resource库
```
npm install vue-resource
```
创建db.json文件并存入数据
```
[
  {
    "name": "ZhangSan",
    "age": 28
  },
  {
    "name": "Allen",
    "age": 27
  }
]
```
案例：通过Vue-resource实现Ajax请求
```
<body>
<div id="app">
  <h2>{{response_data}}</h2>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script src="node_modules/vue-resource/dist/vue-resource.js"></script>
<script>
  var vm=new Vue({
      data(){
          return{
              response_data:[]
          }
      },
      created(){
        this.$http.get('http://localhost:63342/vue-demo/db.html?_ijt=20946e4dgtscae9r9l0amjlsk6').then((response)=>{
            //如果要使用Vue实例的this，此处必须使用箭头函数
            console.log('db.json',response.data) //响应数据
            this.response_data=response.data   //将响应数据赋值给response_data
        },(response)=>{
            //error callback
            console.log('error info',response.statusText) //返回错误信息
        })
      }
  }).$mount('#app')
</script>
```
>axios

安装axios
```
npm install axios
```
案例：
```
<body>
<div id="app">
  <h2>{{response_data}}</h2>
</div>
</body>
<script src="node_modules/vue/dist/vue.js"></script>
<script src="node_modules/axios/dist/axios.js"></script>
<script>
  var vm=new Vue({
      data(){
          return{
              response_data:[]
          }
      },
      created(){
        axios.get('http://localhost:63342/vue-demo/db.html?_ijt=20946e4dgtscae9r9l0amjlsk6').then((response)=>{
            //如果要使用Vue实例的this，此处必须使用箭头函数
            console.log('db.json',response.data) //响应数据
            this.response_data=response.data   //将响应数据赋值给response_data
        },(response)=>{
            //error callback
            console.log('error info',response.statusText) //返回错误信息
        })
      }
  }).$mount('#app')
</script>
```
# Vue-Router路由
>什么是路由？

Vue Router是Vue.js官方的路由管理器，它和Vue.js的核心深度集成，让构建单页面变得非常简单。
通过根据不同的请求路径，切换显示不同组件进行渲染页面。
## 基本使用
安装vue-router模块
```
npm install vue-router
```
引入vue-router.js
```
<script src="../node_modules/vue/dist/vue.js"></script>
<script src="../node_modules/vue-router/dist/vue-router.global.js"></script>
```
>vue-router应用案例

```
<body>
  <div id="app">
    <div id="point">
<!--      方式1-->
      <div><a href="#/foo">foo</a></div>
      <div><a href="#/bar">bar</a></div>
<!--      方式2-->
      <div><router-link to="/foo">go foo</router-link></div>
      <div><router-link to="/bar">go bar</router-link></div>
    </div>
    <div id="main">
<!--      路由出口，匹配的组件将会渲染在这里-->
      <router-view></router-view>
    </div>
  </div>
</body>
<script src="../node_modules/vue/dist/vue.js"></script>
<script src="../node_modules/vue-router/dist/vue-router.js"></script>
<script>
<!--定义组件-->
var Foo={
    template:'<div>foo组件</div>'
}
var Bar={
    template:'<div>bar组件</div>'
}
//配置路由，当点击指定url时显示对应组件
const router=new VueRouter({
    routes:[
        {path:'/foo',component:Foo},
        {path: '/bar',component: Bar}
    ]
})
//路由绑定到vue实例
new Vue({
    el:'#app',
    router // router:router
})
</script>
```
# router-link样式高亮
>router-link常用属性

|属性|描述|
|:---|:---|
|tag|router-link标签默认渲染成a标签，使用tag属性可以指定渲染生成其他标签；<router-link tag="li">foo</router-link>|
|active-class|router-link标签默认渲染router-link-active类名，使用active-class属性指定渲染生成其他css；可以通过linkActiveClass来全局配置，不用在每个router-link使用active-class指定生成其他类名|
|exact|默认情况下路由地址/,/foo,/bar都以/开头他们都会匹配/地址的css类名，可以在router-link上使用exact属性开启css类名精确匹配；<router-link exact></router-link>|

router实例代码
```
;(function(){
    window.router = new VueRouter({
        linkActiveClass:'active'
    })
})()
```
# 嵌套路由
>配置嵌套路由

```
{
    path:'/news',
    component: News,
    children:[
        {
            path:'/news/sport',
            component:Sport
        },
        {
            path:'tech',
            component:Tech
        }
        
    ]
}
```
# 缓存路由组件
将用户输入数据缓存
```
<keep-alive>
    <router-view></router-view>
</keep-alive>
```
# 编程式路由导航
>声明式路由

```
声明式直接通过a标签href指定链接跳转
<router-link :to="...">
```
>编程式路由导航

```
编程式采用js代码链接跳转如localhost.href
router.push(...)
```